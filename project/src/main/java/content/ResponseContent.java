package content;

import commands.Direction;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ResponseContent {
    private Map<String, Object> sessionAttributes;
    private Map<String, Object> requestAttributes;
    private Map<String, Object> requestParameters;
    private Direction direction;
    private String page;
    private boolean isInvalidated;

    public ResponseContent() {
        sessionAttributes = new HashMap<>();
        requestAttributes = new HashMap<>();
        requestParameters = new HashMap<>();
    }

    public void addSessionAttribute(String key, Object value) {
        sessionAttributes.put(key, value);
    }

    public void addRequestAttribute(String key, Object value) {
        requestAttributes.put(key, value);
    }

    public void addRequestParameter(String key, Object value) {
        requestParameters.put(key, value);
    }

    public Map<String, Object> getRequestAttributes() {
        return requestAttributes;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void updateRequest(HttpServletRequest request) {
        requestAttributes.forEach(request::setAttribute);
        sessionAttributes.forEach(request.getSession()::setAttribute);
    }

    public Map<String, Object> getSessionAttributes() {
        return sessionAttributes;
    }

    public void addParametersToPage() {
        requestParameters.forEach((String key, Object value) -> {
            page = page.concat(key).concat("=").concat((String) value).concat("&");
        });
    }

    public boolean isInvalidated() {
        return isInvalidated;
    }

    public void invalidateSession() {
        isInvalidated = true;
    }
}
