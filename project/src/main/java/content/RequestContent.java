package content;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class RequestContent {
    private Map<String, Object> requestAttributes;
    private Map<String, Object> sessionAttributes;
    private Map<String, String[]> requestParameterMap;
    private HttpServletRequest request;

    public RequestContent(HttpServletRequest req) {
        requestAttributes = new HashMap<>();
        sessionAttributes = new HashMap<>();
        requestParameterMap = req.getParameterMap();
        this.request = req;

        Enumeration<String> requestAttributeNames = req.getAttributeNames();
        while(requestAttributeNames.hasMoreElements()) {
            String requestAttributeName = requestAttributeNames.nextElement();
            requestAttributes.put(requestAttributeName, req.getAttribute(requestAttributeName));
        }
        HttpSession currentSession = req.getSession(false);
        if(currentSession != null) {
            Enumeration<String> sessionAttributeNames = currentSession.getAttributeNames();
            while(sessionAttributeNames.hasMoreElements()) {
                String sessionAttributeName = sessionAttributeNames.nextElement();
                sessionAttributes.put(sessionAttributeName, req.getAttribute(sessionAttributeName));
            }
        }
    }

    public Map<String, Object> getRequestAttributes() {
        return requestAttributes;
    }

    public Map<String, Object> getSessionAttributes() {
        return sessionAttributes;
    }

    public Map<String, String[]> getRequestParameterMap() {
        return requestParameterMap;
    }

    public Object getRequestAttribute(String attributeName) {
        return requestAttributes.get(attributeName);
    }

    public Object getSessionAttribute(String attributeName) {
        return sessionAttributes.get(attributeName);
    }

    public String[] getRequestParameters(String parameterName){
        return requestParameterMap.get(parameterName);
    }

    public boolean isRequestAttributeExist(String attributeName) {
        return requestAttributes.containsKey(attributeName);
    }

    public boolean isSessionAttributeExist(String attributeName) {
        return sessionAttributes.containsKey(attributeName);
    }

    public boolean isRequestParameterExist(String parameterName) {
        return requestParameterMap.containsKey(parameterName);
    }

    public HttpServletRequest getRequest() {
        return request;
    }
}
