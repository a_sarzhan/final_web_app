package validation;

import entity.User;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidator {
    private static final Logger LOGGER = LogManager.getLogger(InputValidator.class);
    private static final Pattern EMAIL_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static InputValidator validator;

    private InputValidator() {
    }

    public static InputValidator getValidator() {
        if (validator == null) {
            validator = new InputValidator();
        }
        return validator;
    }

    public boolean isValidUserInput(User user) {
        if (isValidUserLogin(user.getUserLogin()) && isValidUserPassword(user.getUserPassword())
                && isValidUserIIN(user.getUserIIN()) && isValidUserName(user.getUserFirstName())
                && isValidUserName(user.getUserLastName()) && isValidUserEmail(user.getUserEmail())
                && isValidUserPhone(user.getUserPhone())) {
            LOGGER.log(Level.INFO, "User Input Validation success");
            return true;
        }
        LOGGER.log(Level.INFO, "User Input Validation success");
        return false;
    }

    private boolean isValidUserLogin(String login) {
        String regex = "^[0-9a-zA-Z_.-]+$";
        LOGGER.log(Level.INFO, "VALIDATIN LOGIN " + login + " // " + login.matches(regex));
        return login.matches(regex);
    }

    private boolean isValidUserPassword(String password) {
        String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        LOGGER.log(Level.INFO, "VALIDATIN password " + password + " // " + password.matches(regex));
        return password.matches(regex);
    }

    private boolean isValidUserIIN(String iin) {
        String regex = "^[1-9][0-9]{11}$";
        LOGGER.log(Level.INFO, "VALIDATIN iin " + iin + " // " + iin.matches(regex));
        return iin.matches(regex);
    }

    private boolean isValidUserName(String name) {
        String regex = "^[A-Za-z\\u0400-\\u04FF]+$";
        LOGGER.log(Level.INFO, "VALIDATIN name " + name + " // " + name.matches(regex));
        return name.matches(regex);
    }

    private boolean isValidUserEmail(String email) {
        Matcher matcher = EMAIL_REGEX.matcher(email);
        return matcher.find();
    }

    private boolean isValidUserPhone(String phone) {
        String regex = "^[\\+][7]{2}[0-9]{9}$";
        LOGGER.log(Level.INFO, "VALIDATIN phone " + phone + " // " + phone.matches(regex));
        return phone.matches(regex);
    }

    public boolean isValidCreditRequestFields(int sum, int period, double salary) {
        return isValidSum(sum) && isValidPeriod(period) && isValidSalary(salary, sum);
    }

    private boolean isValidSum(int sum) {
        return sum >= 100_000 && sum <= 7000_000;
    }

    private boolean isValidPeriod(int period) {
        return period >= 3 && period <= 72;
    }

    private boolean isValidSalary(double salary, int sum) {
        return salary < sum;
    }
}
