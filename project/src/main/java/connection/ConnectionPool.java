package connection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayDeque;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private static ConnectionPool poolInstance;
    private static ReentrantLock lock = new ReentrantLock();
    private static ReentrantLock dequeLock = new ReentrantLock();
    private static AtomicBoolean instanceExist = new AtomicBoolean(false);
    private static final int POOL_SIZE = 10;
    private static Driver mySQLDriver;
    private ArrayDeque<ProxyConnection> freeDeque;
    private ArrayDeque<ProxyConnection> busyDeque;

    private ConnectionPool() {
    }

    public static ConnectionPool getInstance() {
        if (!instanceExist.get()) {
            lock.lock();
            try {
                if (poolInstance == null) {
                    poolInstance = new ConnectionPool();
                    //LOGGER.info("before initializePool");
                    poolInstance.initializePool();
                    //LOGGER.info("after initializePool");
                    instanceExist.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return poolInstance;
    }

    private void initializePool() {
        try {
            mySQLDriver = new com.mysql.jdbc.Driver();
            DriverManager.registerDriver(mySQLDriver);
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, "Error while registering MYSQL Driver. " + err.getErrorCode() + " // " + err.getMessage());
        }
        freeDeque = new ArrayDeque<ProxyConnection>(POOL_SIZE);
        busyDeque = new ArrayDeque<ProxyConnection>(POOL_SIZE);
        for (int i = 0; i < POOL_SIZE; i++) {
            //LOGGER.info("i = " + i + " POOL_ZIE=" + POOL_SIZE);
            freeDeque.push(createNewConnectionToPool());
        }
    }

    private ProxyConnection createNewConnectionToPool() {
        //LOGGER.info("in createNewConnectionToPool");
        ProxyConnection connection = null;
        ResourceBundle resource = ResourceBundle.getBundle("dbConfig");
        String user = resource.getString("user");
        String password = resource.getString("password");
        String dbName = resource.getString("dbName");
        String host = resource.getString("host");
        String port = resource.getString("port");
        String useUnicode = resource.getString("useUnicode");
        String encoding = resource.getString("characterEncoding");
        String url = "jdbc:mysql://" + host + ":" + port + "/" + dbName + "?useUnicode=" + useUnicode
                + "&characterEncoding=" + encoding;
        try {
            connection = new ProxyConnection(DriverManager.getConnection(url, user, password));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, "Error while getting connection. " + err.getErrorCode() + " // " + err.getMessage());
        }

        return connection;
    }

    public ProxyConnection getConnectionFromPool() {
        ProxyConnection connection = null;
        dequeLock.lock();
        try {
            connection = freeDeque.pop();
            busyDeque.push(connection);
            LOGGER.info("Connection GET-> freeDeque size: " + freeDeque.size() + " busyDeque size: " + busyDeque.size());
        } finally {
            dequeLock.unlock();
        }
        return connection;
    }

    public void returnConnectionToPool(ProxyConnection connection) {
        dequeLock.lock();
        try {
            busyDeque.remove(connection);
            freeDeque.push(connection);
            LOGGER.info("Connection RETURN-> freeDeque size: " + freeDeque.size() + " busyDeque size: " + busyDeque.size());
        } finally {
            dequeLock.unlock();
        }
    }
    // destroyPoolConnections
    public void destroyPoolConnections() {
        dequeLock.lock();
        try {
            for (int i = 0; i < freeDeque.size(); i++) {
                freeDeque.peek().closeConnection();
            }
            for (int i = 0; i < busyDeque.size(); i++) {
                busyDeque.peek().closeConnection();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Error occurred while closing all connections of ConnectionPool");
        } finally {
            dequeLock.unlock();
        }
    }
    //deregisterMySQLDriver
    public void deregisterMySQLDriver() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                LOGGER.log(Level.INFO, String.format("deregistering jdbc driver: %s", driver));
            } catch (SQLException e) {
                LOGGER.log(Level.ERROR, String.format("Error deregistering driver %s", driver), e);
            }
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        if (poolInstance != null) {
            throw new CloneNotSupportedException();
        }
        return super.clone();
    }
}
