package receivers;

import content.RequestContent;
import dao.TransactionHelper;
import dao.implementation.ManagerDao;
import dao.implementation.UserDao;
import entity.Manager;
import entity.User;
import exceptions.UserNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class ManagerLogic implements LogicPerformer {
    private static final Logger LOGGER = LogManager.getLogger(UserLogic.class);
    private static final ManagerDao managerDao = new ManagerDao();
    private static final TransactionHelper helper = new TransactionHelper();

    public Manager verifyManager(RequestContent content) throws UserNotFoundException, SQLException {
        String login = content.getRequestParameters("managerId")[0];
        String password = content.getRequestParameters("managerPassword")[0];
        Manager manager = null;
        helper.beginTransaction(managerDao);
        try {
            manager = managerDao.findManagerByLoginPassword(login, password);
            LOGGER.log(Level.INFO, "Manager found by login and password. Manager ID: " + login);
        } finally {
            helper.endTransaction(managerDao);
        }
        return manager;
    }
}
