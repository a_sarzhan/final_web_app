package receivers;

import content.ResponseContent;
import dao.TransactionHelper;
import dao.implementation.AccountDao;
import dao.implementation.CreditRequestDao;
import dao.implementation.TransactionDao;
import entity.CreditRequest;
import exceptions.CreditRequestNotFoundException;
import exceptions.DataNotFoundException;
import exceptions.TransactionException;
import localization.ResourceLocalizer;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class CreditLogic implements LogicPerformer {
    private static final Logger LOGGER = LogManager.getLogger(CreditLogic.class);
    private static CreditRequestDao creditDao = new CreditRequestDao();
    private static final TransactionHelper helper = new TransactionHelper();

    public boolean addCreditRequest(CreditRequest creditRequest) throws SQLException {
        boolean result = false;
        helper.beginTransaction(creditDao);
        try {
            int id = creditDao.getRequestId(creditRequest.getRequestOwner().getUserId(), "active");
            LOGGER.log(Level.INFO, " get request id: " + id + " by userId: " + creditRequest.getRequestOwner().getUserId());
            if (id > 0) {
                LOGGER.log(Level.INFO, "By given userId: " + id + " already ACTIVE credit request exists.");
                result = false;
            } else {
                result = creditDao.addRequestToDB(creditRequest);
                id = creditDao.getRequestId(creditRequest.getRequestOwner().getUserId(), "active");
                LOGGER.log(Level.INFO, "new credit request is added requestID = " + id + " userId: "
                        + creditRequest.getRequestOwner().getUserId());
                creditRequest.setRequestId(id);
            }
            helper.commit();
        } finally {
            helper.endTransaction(creditDao);
        }
        return result;
    }

    //find all credit requests by specified parameters
    public ArrayList<CreditRequest> findCreditRequestsByParameters(LinkedHashMap<String, String> searchMap) throws ParseException, SQLException {
        CreditRequestDao creditDao = new CreditRequestDao();
        helper.beginTransaction(creditDao);
        try {
            LinkedHashMap<String, String> filteredMap = new LinkedHashMap<>();
            for (Map.Entry<String, String> entry : searchMap.entrySet()) {
                if (!entry.getValue().isEmpty()) {
                    LOGGER.log(Level.INFO, "Request search parameters: key is " + entry.getKey() + " value is " + entry.getValue());
                    creditDao.updateSelectAllRequestQuery(entry.getKey());
                    filteredMap.put(entry.getKey(), entry.getValue());
                }
            }
            ArrayList<CreditRequest> requestList = creditDao.findAllCreditRequestsByParameters(filteredMap);
            for (CreditRequest request : requestList) {
                LOGGER.log(Level.INFO, "Search result. Request ID: " + request.getRequestId() + " Client IIN: "
                        + request.getRequestOwner().getUserIIN());
            }
            return requestList;
        } finally {
            helper.endTransaction(creditDao);
        }
    }

    //find all active credit requests
    public ArrayList<CreditRequest> findActiveCreditRequests() throws SQLException {
        helper.beginTransaction(creditDao);
        try {
            return creditDao.findAllActiveCreditRequests();
        } finally {
            helper.endTransaction(creditDao);
        }
    }

    //find user latest credit request by user id
    public CreditRequest findUserCreditRequest(int userId) throws SQLException, CreditRequestNotFoundException {
        helper.beginTransaction(creditDao);
        try {
            CreditRequest creditRequest = creditDao.findCreditRequestByUserId(userId);
            LOGGER.log(Level.INFO, "Latest Credit request of User was found, by user_id: " + userId);
            return creditRequest;
        } finally {
            helper.endTransaction(creditDao);
        }
    }

    //complex business logic of getting credit terms...
    public LinkedHashSet<CreditRequest> getCreditTermsOfNewRequests(LinkedHashSet<CreditRequest> requestSet, ResponseContent response)
            throws DataNotFoundException, SQLException {
        LinkedHashSet<CreditRequest> creditTermsList = new LinkedHashSet<>();
        ClientPayabilityLogic payabilityLogic = ClientPayabilityLogic.getInstance();
        AccountDao accountDao = new AccountDao();
        TransactionDao transactionDao = new TransactionDao();
        boolean termsDefined = false;
        helper.beginTransaction(creditDao, accountDao, transactionDao);
        try {
            for (CreditRequest request : requestSet) {
                String iin = request.getRequestOwner().getUserIIN();
                LOGGER.info(String.valueOf(Level.INFO), "SOS requestId: " + request.getRequestId() + " iin: " + iin);

                if (request.isHasSalaryCard()) {
                    int accountId = accountDao.findAccountIdByIIN(iin);
                    if (accountId > 0) {
                        LOGGER.log(Level.INFO, "Client has MFC salary card as declared. Client IIN: " + iin);
                        Map<String, Double> termsMap = null;
                        try {
                            termsMap = payabilityLogic.getCreditTermsMfcClient(accountId, transactionDao, request);
                            termsDefined = mapCreditTermsToCreditRequestEntity(request, termsMap);
                        } catch (TransactionException e) {
                            CreditRequest resultRequest = getUpdatedCreditRequestStatus(request, false);
                            creditTermsList.add(resultRequest);
                            LOGGER.log(Level.INFO, e.getMessage());
                            response.addSessionAttribute("failReason", ResourceLocalizer.getInstance().getMessage("transaction.notEnough"));
                        }
                    } else {
                        CreditRequest resultRequest = getUpdatedCreditRequestStatus(request, false);
                        creditTermsList.add(resultRequest);
                        LOGGER.log(Level.INFO, "Client's MFC salary card account not found by client IIN: " + iin);
                        response.addSessionAttribute("failReason", ResourceLocalizer.getInstance().getMessage("transaction.cardFail"));
                    }
                } else {
                    try {
                        Map<String, Double> termsMap = payabilityLogic.getCreditTermsOtherClient(request);
                        termsDefined = mapCreditTermsToCreditRequestEntity(request, termsMap);
                    } catch (TransactionException e) {
                        CreditRequest resultRequest = getUpdatedCreditRequestStatus(request, false);
                        creditTermsList.add(resultRequest);
                        LOGGER.log(Level.INFO, e.getMessage());
                        response.addSessionAttribute("failReason", ResourceLocalizer.getInstance().getMessage("transaction.gcvpFail"));
                    }
                }

                CreditRequest resultRequest = getUpdatedCreditRequestStatus(request, termsDefined);
                creditTermsList.add(resultRequest);
            }
            LOGGER.log(Level.INFO, "Getting crediting conditions/terms for user requests succeeded");
            helper.commit();
        } catch (SQLException err) {
            helper.rollback();
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
            throw new SQLException(err);
        } finally {
            helper.endTransaction(creditDao, accountDao, transactionDao);
        }
        return creditTermsList;
    }

    public LinkedHashSet<CreditRequest> getProcessingCreditRequests(String[] requestIdList) throws DataNotFoundException, SQLException,
            NumberFormatException {
        LinkedHashSet<CreditRequest> requestSet = new LinkedHashSet<>();
        for (String stringRequestId : requestIdList) {
            int integerRequestId = Integer.parseInt(stringRequestId);
            CreditRequest creditRequest = creditDao.findCreditRequestById(integerRequestId);
            requestSet.add(creditRequest);
        }
        return requestSet;
    }

    private boolean mapCreditTermsToCreditRequestEntity(CreditRequest creditRequest, Map<String, Double> termsMap) {
        boolean termNotEmpty = false;
        for (Map.Entry<String, Double> entry : termsMap.entrySet()) {
            termNotEmpty = true;
            if (entry.getKey().equals("rate")) {
                creditRequest.setRate(entry.getValue());
            }
            if (entry.getKey().equals("coefficient")) {
                creditRequest.setCommission(entry.getValue());
            }
            if (entry.getKey().equals("monthlyPayment")) {
                creditRequest.setMonthlyPayment(entry.getValue());
            }
        }
        return termNotEmpty;
    }

    private CreditRequest getUpdatedCreditRequestStatus(CreditRequest creditRequest, boolean decision)
            throws SQLException, DataNotFoundException {
        String status = decision ? "success" : "fail";
        int requestId = creditRequest.getRequestId();
        if (creditDao.updateRequestStatusByRequestId(creditRequest, status)) {
            return creditDao.findCreditRequestById(requestId);
        } else {
            return creditRequest;
        }
    }

    public CreditRequest getUpdatedRequestWithDecision(int requestId, String clientDecision)
            throws CreditRequestNotFoundException, SQLException {
        CreditRequest creditRequest = null;
        helper.beginTransaction(creditDao);
        try {
            creditDao.updateRequestDecisionByRequestId(requestId, clientDecision);
            creditRequest = creditDao.findCreditRequestById(requestId);
            helper.commit();
        } catch (DataNotFoundException e) {
            throw new CreditRequestNotFoundException("Credit Request not found by request_id: " + requestId);
        } finally {
            helper.endTransaction(creditDao);
        }
        return creditRequest;
    }

    public CreditRequest getUpdatedCreditRequest(CreditRequest creditRequest) throws DataNotFoundException, SQLException {
        CreditRequest credit = creditRequest;
        helper.beginTransaction(creditDao);
        try {
            creditDao.updateCreditRequest(creditRequest);
            credit = creditDao.findCreditRequestById(creditRequest.getRequestId());
            helper.commit();
        } finally {
            helper.endTransaction(creditDao);
        }
        return credit;
    }

    public boolean isDeletedCreditRequestById(int id) throws SQLException {
        boolean isDeleted = false;
        helper.beginTransaction(creditDao);
        try {
            isDeleted = creditDao.deleteCreditRequestById(id);
            helper.commit();
        } finally {
            helper.endTransaction(creditDao);
        }
        return isDeleted;
    }

    public CreditRequest getCreditRequestByRequestId(int id) throws SQLException, DataNotFoundException {
        CreditRequest resultRequest = null;
        helper.beginTransaction(creditDao);
        try {
            resultRequest = creditDao.findCreditRequestById(id);
        } finally {
            helper.endTransaction(creditDao);
        }
        return resultRequest;
    }
}
