package receivers;

import content.RequestContent;
import dao.TransactionHelper;
import dao.implementation.UserDao;
import entity.User;
import exceptions.DataNotFoundException;
import exceptions.UserNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class UserLogic implements LogicPerformer {
    private static final Logger LOGGER = LogManager.getLogger(UserLogic.class);
    private static final UserDao userDao = new UserDao();
    private static final TransactionHelper helper = new TransactionHelper();

    public User verifyUser(RequestContent content) throws UserNotFoundException, SQLException {
        String login = content.getRequestParameters("userName")[0];
        String password = content.getRequestParameters("password")[0];
        User user = null;
        helper.beginTransaction(userDao);
        try {
            user = userDao.findUserByNamePassword(login, password);
            LOGGER.log(Level.INFO, "User found by login and password. login: " + login);
        } finally {
            helper.endTransaction(userDao);
        }
        return user;
    }

    public User getUpdatedUserEntity(User userFromJsp) throws UserNotFoundException, SQLException {
        User user = userFromJsp;
        helper.beginTransaction(userDao);
        try {
            if (userDao.hasUserInfoUpdated(userFromJsp)) {
                LOGGER.log(Level.INFO, "User info successfully updated");
                user = userDao.findUserById(userFromJsp.getUserId());
            }
            helper.commit();
        } catch (DataNotFoundException e) {
            throw new UserNotFoundException("Client/User not found in Database by user_id: " + userFromJsp.getUserId());
        } finally {
            helper.endTransaction(userDao);
        }
        return user;
    }

    public boolean addNewUser(User user) throws SQLException {
        boolean result = false;
        helper.beginTransaction(userDao);
        try {
            result = userDao.addUserToDB(user);
            helper.commit();
        } finally {
            helper.endTransaction(userDao);
        }
        return result;
    }

    public int getUserIdByLogin(String login) throws SQLException {
        int userId;
        helper.beginTransaction(userDao);
        try {
            userId = userDao.retrieveUserIdByLogin(login);
        } finally {
            helper.endTransaction(userDao);
        }
        return userId;
    }
}
