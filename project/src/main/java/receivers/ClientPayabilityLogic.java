package receivers;

import dao.implementation.TransactionDao;
import entity.CreditRequest;
import exceptions.TransactionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.*;

public class ClientPayabilityLogic implements LogicPerformer {
    private static final Logger LOGGER = LogManager.getLogger(ClientPayabilityLogic.class);
    private static final LinkedHashMap<String, Double> rateMap;
    private static final LinkedHashMap<String, Double> commissionMap;
    private static ClientPayabilityLogic payabilityLogic;

    static {
        rateMap = new LinkedHashMap<>();
        rateMap.put("50000-100000", 19.0);
        rateMap.put("100000-250000", 18.3);
        rateMap.put("250000-500000", 17.4);
        rateMap.put("500000-5000000", 16.8);

        commissionMap = new LinkedHashMap<>();
        commissionMap.put("positive", 0.5);
        commissionMap.put("satisfactory", 0.7);
        commissionMap.put("dubious", 0.9);
        commissionMap.put("negative", 1.2);
    }

    private ClientPayabilityLogic() {
    }

    public static ClientPayabilityLogic getInstance() {
        if (payabilityLogic == null) {
            payabilityLogic = new ClientPayabilityLogic();
        }
        return payabilityLogic;
    }

    Map<String, Double> getCreditTermsMfcClient(int account_id, TransactionDao dao, CreditRequest creditRequest)
            throws TransactionException, SQLException {
        Map<String, Double> termsMap = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -6);
        Date date = cal.getTime();
        //get amount of transaction for the last 6 month
        int transactionCount = dao.getTransactionsCountByDate(account_id, date);
        LOGGER.info("Client has " + transactionCount + " transactions in last 6 months");
        if (transactionCount > 3) {
            double calculatedSalary = dao.getTransactionSumByDate(account_id, date) / 6;
            double rate = getCreditRate(calculatedSalary);
            double commission = getCreditCommission();
            double monthlyPayment = getCalculatedMonthlyPayment(creditRequest, rate);
            termsMap.put("rate", rate);
            termsMap.put("coefficient", commission);
            termsMap.put("monthlyPayment", monthlyPayment);
        } else {
            LOGGER.log(Level.INFO, " Not enough transactions for the past 6 months");
            throw new TransactionException("Not enough transactions for the past 6 months");
        }
        LOGGER.log(Level.INFO, " Client has more than 3 transactions for the past 6 months");
        return termsMap;
    }

    private double getCalculatedMonthlyPayment(CreditRequest request, double rate) {
        double monthlyPayment;
        double creditSum = request.getCreditSumm();
        int creditMonth = request.getCreditMonth();
        double monthlyRate =  rate / 12.0;
        double paymentCoefficient = (monthlyRate * Math.pow((1 + monthlyRate), creditMonth)) /
                (Math.pow((1 + monthlyRate), creditMonth) - 1);
        monthlyPayment = creditSum * paymentCoefficient;
        monthlyPayment = Math.round(monthlyPayment * 100.0) / 100.0;
        LOGGER.info("Defined credit monthly payment:  " + monthlyPayment);
        return monthlyPayment;
    }

    //works randomly just for project
    private String getCreditHistory() {
        String creditHistory = "positive";
        Random randomIndex = new Random();
        int commissionIndex = randomIndex.nextInt(commissionMap.size());
        int index = 0;
        for (String history : commissionMap.keySet()) {
            if (index == commissionIndex) {
                creditHistory = history;
            }
            index++;
        }
        return creditHistory;
    }

    private double getCreditRate(double salary) throws TransactionException {
        String[] boundary;
        double lowerBound;
        double upperBound;
        double rate = 0;
        for (Map.Entry<String, Double> entry : rateMap.entrySet()) {
            boundary = entry.getKey().split("-");
            lowerBound = Double.parseDouble(boundary[0]);
            upperBound = Double.parseDouble(boundary[1]);
            if (salary >= lowerBound && salary < upperBound) {
                rate = entry.getValue() / 100.0;
            }
        }
        if (rate == 0) {
            throw new TransactionException("Invalid client salary");
        }
        LOGGER.log(Level.INFO, "Defined rate for credit: " + rate);
        return rate;
    }

    private double getCreditCommission() {
        String creditHistory = getCreditHistory();
        double commission = 0;
        for (Map.Entry<String, Double> entry : commissionMap.entrySet()) {
            if (entry.getKey().equals(creditHistory)) {
                commission =  entry.getValue() * 100.0 / 10000.0;
                LOGGER.info("Defined credit commission is: " + commission);
                break;
            }
        }
        return commission;
    }

    Map<String, Double> getCreditTermsOtherClient(CreditRequest creditRequest) throws TransactionException {
        Map<String, Double> termsMap = new HashMap<>();
        if (isClientSalaryVerifiedByGCVP()) {
            double salary = creditRequest.getClientSalary();
            double rate = getCreditRate(salary);
            double commission = getCreditCommission();
            double monthlyPayment = getCalculatedMonthlyPayment(creditRequest, rate);
            termsMap.put("rate", rate);
            termsMap.put("coefficient", commission);
            termsMap.put("monthlyPayment", monthlyPayment);
        } else {
            LOGGER.log(Level.INFO,"Client salary external verification failed ");
            throw new TransactionException("Client salary external verification failed");
        }
        LOGGER.log(Level.INFO,"Client salary externally verified");
        return termsMap;
    }

    private boolean isClientSalaryVerifiedByGCVP() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
