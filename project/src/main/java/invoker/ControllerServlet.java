package invoker;

import commands.AbstractCommand;
import commands.Direction;
import commands.factory.CommandFactory;
import connection.ConnectionPool;
import content.RequestContent;
import content.ResponseContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/controller")
public class ControllerServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ControllerServlet.class);

    public ControllerServlet() {
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Servlet GET");
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Servlet POST");
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestContent content = new RequestContent(req);
        AbstractCommand command = CommandFactory.defineCommand(content);
        ResponseContent result = command.execute(content);
        if (result.isInvalidated()) {
            LOGGER.info("session invalidate in Servlet");
            resp.sendRedirect(result.getPage());
            req.getSession(false).invalidate();
            return;
        }
        result.updateRequest(req);

        if (result.getDirection() == Direction.FORWARD) {
            LOGGER.info("page is " + result.getPage() + " forward action");
            req.getRequestDispatcher(result.getPage()).forward(req, resp);
        }
        if (result.getDirection() == Direction.REDIRECT) {
            LOGGER.info("page is " + result.getPage() + " redirect action");
            resp.sendRedirect(result.getPage());
        }
        //LOGGER.info("end of Servlet");
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().destroyPoolConnections();
        ConnectionPool.getInstance().deregisterMySQLDriver();
    }
}
