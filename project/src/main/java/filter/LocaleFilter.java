package filter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Locale;

public class LocaleFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger(LocaleFilter.class);
    private String localeCode;
    private boolean ignore = false;
    private FilterConfig filterConfig;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        localeCode = filterConfig.getInitParameter("locale");
        ignore = Boolean.parseBoolean(filterConfig.getInitParameter("ignore"));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession();
        Locale currentLocale = (Locale) session.getAttribute("locale");
        //LOG.log(Level.INFO, " filter currentLocale is " + currentLocale);
        Locale.setDefault(Locale.US);
        if(currentLocale == null || ignore) {
            Locale locale = new Locale(localeCode);
            session.setAttribute("locale", locale);
            Config.set(session, Config.FMT_LOCALE, locale);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        // nothing todo
    }
}
