package filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NoCacheFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(NoCacheFilter.class);

    // private FilterConfig filterConfig;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        //LOGGER.log(Level.INFO, "before cache filter");

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setDateHeader("Expires", 0); // Proxies.

        filterChain.doFilter(servletRequest, servletResponse);
        //LOGGER.log(Level.INFO, "after cache filter");

    }

    @Override
    public void destroy() {

    }
}
