package dao.implementation;

import dao.AbstractDao;
import entity.CreditRequest;
import entity.User;
import exceptions.CreditRequestNotFoundException;
import exceptions.DataNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class CreditRequestDao extends AbstractDao<CreditRequest> {
    private static final Logger LOGGER = LogManager.getLogger(CreditRequestDao.class);
    private static final String SQL_SELECT_REQUEST_ID = "SELECT request_id FROM finance.creditRequests " +
            "WHERE user_id = ? AND request_status = ?;";
    private static final String SQL_ADD_NEW_REQUEST = "INSERT INTO finance.creditRequests (credit_summ, " +
            "credit_month, salary_card, client_salary, request_date, request_status, user_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?);";
    //SQL_SELECT_REQUEST_CONSTANT is used not to repeat query each time
    private static final String SQL_SELECT_REQUEST_CONSTANT = "SELECT cr.request_id, cr.credit_summ, cr.credit_month, " +
            "cr.salary_card, cr.client_salary, cr.request_date, cr.request_status, cr.rate, cr.commission, " +
            "cr.monthly_payment, cr.client_decision, u.user_id, u.user_IIN, u.user_name, u.user_lastname " +
            "FROM finance.creditRequests cr, finance.users u " +
            "WHERE cr.user_id = u.user_id";
    private static final String SQL_SELECT_ACTIVE_REQUESTS = SQL_SELECT_REQUEST_CONSTANT + " AND cr.request_status = ?;";
    private static final String SQL_SELECT_REQUEST_BY_ID = SQL_SELECT_REQUEST_CONSTANT + " AND cr.request_id = ?;";
    private static final String SQL_SELECT_REQUEST_BY_USER_ID = SQL_SELECT_REQUEST_CONSTANT + " AND cr.user_id = ? " +
            "AND client_decision IS NULL " +
            "ORDER BY FIELD(cr.request_status, ?, ?, ?, ?) LIMIT 1;";
    private static final String SQL_UPDATE_REQUEST_STATUS = "UPDATE finance.creditRequests SET request_status = ?, " +
            "rate = ?, commission = ?, monthly_payment = ? WHERE request_id = ?;";
    private static final String SQL_UPDATE_REQUEST_DECISION = "UPDATE finance.creditRequests SET client_decision = ? " +
            "WHERE request_id = ?;";
    private static final String SQL_UPDATE_CREDIT_REQUEST = "UPDATE finance.creditRequests SET credit_summ = ?, " +
            "credit_month = ?, salary_card = ?, client_salary = ? WHERE request_id = ?;";
    private static final String SQL_DELETE_CREDIT_REQUEST = "DELETE FROM finance.creditRequests WHERE request_id = ?;";
    private String SQL_SELECT_ALL_REQUEST = SQL_SELECT_REQUEST_CONSTANT;

    public int getRequestId(int userId, String status) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_REQUEST_ID)) {
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, status);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("request_id");
            } else {
                return 0;
            }
        }
    }

    public void updateSelectAllRequestQuery(String parameter) {
        switch (parameter) {
            case "IIN":
                SQL_SELECT_ALL_REQUEST += " AND u.user_IIN = ?";
                break;
            case "requestDateFrom":
                SQL_SELECT_ALL_REQUEST += " AND cr.request_date >= ?";
                break;
            case "requestDateTo":
                SQL_SELECT_ALL_REQUEST += " AND cr.request_date <= ?";
                break;
            case "requestStatus":
                SQL_SELECT_ALL_REQUEST += " AND cr.request_status = ?";
                break;
            case "clientDecision":
                SQL_SELECT_ALL_REQUEST += " AND cr.client_decision = ?";
                break;
        }
        LOGGER.log(Level.INFO, "QUERY IS " + SQL_SELECT_ALL_REQUEST);
    }

    public ArrayList<CreditRequest> findAllCreditRequestsByParameters(LinkedHashMap<String, String> map) throws ParseException, SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_REQUEST)) {
            ArrayList<CreditRequest> requestsList = new ArrayList<>();
            setSelectAllRequestQueryParameters(map, preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CreditRequest creditRequest = new CreditRequest();
                mapToEntity(resultSet, creditRequest);
                requestsList.add(creditRequest);
            }
            return requestsList;
        }
    }

    public boolean updateRequestStatusByRequestId(CreditRequest request, String status) throws SQLException {
        double rate = request.getRate();
        double commission = request.getCommission();
        double monthlyPayment = request.getMonthlyPayment();
        int requestId = request.getRequestId();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_REQUEST_STATUS)) {
            preparedStatement.setString(1, status);
            preparedStatement.setDouble(2, rate);
            preparedStatement.setDouble(3, commission);
            preparedStatement.setDouble(4, monthlyPayment);
            preparedStatement.setInt(5, requestId);
            return preparedStatement.executeUpdate() > 0;
        }
    }

    public void updateRequestDecisionByRequestId(int id, String decision) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_REQUEST_DECISION)) {
            preparedStatement.setString(1, decision);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        }
    }

    public void updateCreditRequest(CreditRequest creditRequest) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_CREDIT_REQUEST)) {
            preparedStatement.setInt(1, creditRequest.getCreditSumm());
            preparedStatement.setInt(2, creditRequest.getCreditMonth());
            preparedStatement.setBoolean(3, creditRequest.isHasSalaryCard());
            preparedStatement.setDouble(4, creditRequest.getClientSalary());
            preparedStatement.setInt(5, creditRequest.getRequestId());
            preparedStatement.executeUpdate();
        }
    }

    public ArrayList<CreditRequest> findAllActiveCreditRequests() throws SQLException {
        ArrayList<CreditRequest> requestsList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ACTIVE_REQUESTS)) {
            preparedStatement.setString(1, "active");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CreditRequest creditRequest = new CreditRequest();
                mapToEntity(resultSet, creditRequest);
                requestsList.add(creditRequest);
            }
            return requestsList;
        }
    }

    public CreditRequest findCreditRequestById(int id) throws DataNotFoundException, SQLException {
        return findById(id, SQL_SELECT_REQUEST_BY_ID, CreditRequest.class);
    }

    public CreditRequest findCreditRequestByUserId(int id) throws SQLException, CreditRequestNotFoundException {
        //return findById(id, SQL_SELECT_REQUEST_BY_USER_ID, CreditRequest.class);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_REQUEST_BY_USER_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, "active");
            preparedStatement.setString(3, "success");
            preparedStatement.setString(4, "fail");
            preparedStatement.setString(5, "inactive");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                LOGGER.log(Level.INFO, " ****result is" + resultSet.getInt("request_id"));
                CreditRequest creditRequest = new CreditRequest();
                mapToEntity(resultSet, creditRequest);
                return creditRequest;
            } else {
                throw new CreditRequestNotFoundException("User doesn't have any credit requests");
            }
        }
    }

    public boolean deleteCreditRequestById(int id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_CREDIT_REQUEST)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate() > 0;
        }
    }

    private void setSelectAllRequestQueryParameters(LinkedHashMap<String, String> linkedMap, PreparedStatement statement) throws SQLException, ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        int index = 1;
        for (Map.Entry<String, String> entry : linkedMap.entrySet()) {
            if (entry.getKey().equals("IIN") || entry.getKey().equals("requestStatus")
                    || entry.getKey().equals("clientDecision")) {
                statement.setString(index, entry.getValue());
            }
            if (entry.getKey().equals("requestDateFrom") || entry.getKey().equals("requestDateTo")) {
                Date date = dateFormat.parse(entry.getValue());
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                statement.setDate(index, sqlDate);
            }
            index++;
        }
    }

    public boolean addRequestToDB(CreditRequest creditRequest) throws SQLException {
        return addToDB(creditRequest, SQL_ADD_NEW_REQUEST);
    }

    @Override
    protected void mapToDB(CreditRequest creditRequest, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, creditRequest.getCreditSumm());
        preparedStatement.setInt(2, creditRequest.getCreditMonth());
        preparedStatement.setBoolean(3, creditRequest.isHasSalaryCard());
        preparedStatement.setDouble(4, creditRequest.getClientSalary());
        java.sql.Date sqlRequestDate = new java.sql.Date(creditRequest.getRequestDate().getTime());
        preparedStatement.setDate(5, sqlRequestDate);
        preparedStatement.setString(6, creditRequest.getRequestStatus());
        preparedStatement.setInt(7, creditRequest.getRequestOwner().getUserId());
    }

    @Override
    protected void mapToEntity(ResultSet resultSet, CreditRequest creditRequest) throws SQLException {
        User user = new User();
        user.setUserId(resultSet.getInt("user_id"));
        user.setUserIIN(resultSet.getString("user_IIN"));
        user.setUserFirstName(resultSet.getString("user_name"));
        user.setUserLastName(resultSet.getString("user_lastname"));
        creditRequest.setRequestId(resultSet.getInt("request_id"));
        creditRequest.setCreditSumm(resultSet.getInt("credit_summ"));
        creditRequest.setCreditMonth(resultSet.getInt("credit_month"));
        creditRequest.setClientSalary(resultSet.getDouble("client_salary"));
        creditRequest.setRequestDate(resultSet.getDate("request_date"));
        creditRequest.setHasSalaryCard(resultSet.getBoolean("salary_card"));
        creditRequest.setRequestStatus(resultSet.getString("request_status"));
        creditRequest.setRate(resultSet.getFloat("rate"));
        creditRequest.setCommission(resultSet.getFloat("commission"));
        creditRequest.setMonthlyPayment(resultSet.getDouble("monthly_payment"));
        creditRequest.setClientDecision(resultSet.getString("client_decision"));
        creditRequest.setRequestOwner(user);
    }
}
