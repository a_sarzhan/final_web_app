package dao.implementation;

import dao.AbstractDao;
import entity.User;
import exceptions.DataNotFoundException;
import exceptions.UserNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao extends AbstractDao<User> {
    private static final Logger LOGGER = LogManager.getLogger(UserDao.class);
    private static final String SQL_SELECT_BY_NAME_PASSWORD = "SELECT user_id, user_login, " +
            "user_password, user_IIN, user_name, user_lastname, user_email, user_phone " +
            "FROM finance.users WHERE user_login = ? AND user_password = ?;";
    private static final String SQL_SELECT_BY_ID = "SELECT user_id, user_login, " +
            "user_password, user_IIN, user_name, user_lastname, user_email, user_phone " +
            "FROM finance.users WHERE user_id = ?;";
    private static final String SQL_ADD_NEW_USER = "INSERT INTO finance.users (user_login," +
            "user_password, user_IIN, user_name, user_lastname, user_email, user_phone) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?);";
    private static final String SQL_SELECT_USERID = "SELECT user_id FROM finance.users " +
            "WHERE user_login = ?;";
    private static final String SQL_USER_INFO_UPDATE = "UPDATE finance.users SET user_login = ?, " +
            "user_password = ?, user_IIN = ?, user_name = ?, user_lastname = ?, user_email = ?, user_phone = ? " +
            "WHERE user_id = ?;";

    public User findUserByNamePassword(String name, String password) throws UserNotFoundException, SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_NAME_PASSWORD)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                mapToEntity(resultSet, user);
                return user;
            } else {
                throw new UserNotFoundException("User not found by given login: " + name);
            }
        }
    }

    public boolean hasUserInfoUpdated(User userFromJsp) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_USER_INFO_UPDATE)) {
            mapToDB(userFromJsp, preparedStatement);
            preparedStatement.setInt(8, userFromJsp.getUserId());
            return preparedStatement.executeUpdate() > 0;
        }
    }

    public User findUserById(int userId) throws DataNotFoundException, SQLException {
        return findById(userId, SQL_SELECT_BY_ID, User.class);
    }

    public int retrieveUserIdByLogin(String login) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USERID)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                LOGGER.log(Level.INFO, "User data found user_id is: " + resultSet.getInt("user_id")
                        + " by login: " + login);
                return resultSet.getInt("user_id");
            } else {
                LOGGER.log(Level.INFO, "User data not found by login: " + login);
                return 0;
            }
        }
    }

    public boolean addUserToDB(User user) throws SQLException {
        return addToDB(user, SQL_ADD_NEW_USER);
    }

    @Override
    protected void mapToDB(User user, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, user.getUserLogin());
        preparedStatement.setString(2, user.getUserPassword());
        preparedStatement.setString(3, user.getUserIIN());
        preparedStatement.setString(4, user.getUserFirstName());
        preparedStatement.setString(5, user.getUserLastName());
        preparedStatement.setString(6, user.getUserEmail());
        preparedStatement.setString(7, user.getUserPhone());
    }

    @Override
    protected void mapToEntity(ResultSet resultSet, User user) throws SQLException {
        user.setUserId(resultSet.getInt("user_id"));
        user.setUserLogin(resultSet.getString("user_login"));
        user.setUserPassword(resultSet.getString("user_password"));
        user.setUserIIN(resultSet.getString("user_IIN"));
        user.setUserFirstName(resultSet.getString("user_name"));
        user.setUserLastName(resultSet.getString("user_lastname"));
        user.setUserEmail(resultSet.getString("user_email"));
        user.setUserPhone(resultSet.getString("user_phone"));
    }

}