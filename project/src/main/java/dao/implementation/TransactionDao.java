package dao.implementation;

import dao.AbstractDao;
import entity.Transaction;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class TransactionDao extends AbstractDao<Transaction> {
    private static final Logger LOGGER = LogManager.getLogger(TransactionDao.class);
    private static final String SQL_COUNT_TRANSACTION = "SELECT COUNT(1) FROM finance.transactions " +
            "WHERE account_id = ? and transaction_date >= ?;";
    private static final String SQL_SUM_INCOME = "SELECT SUM(transaction_sum) as salary FROM finance.transactions " +
            "WHERE account_id = ? and transaction_date >= ?;";

    public int getTransactionsCountByDate(int accountId, Date date) throws SQLException {
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_COUNT_TRANSACTION)) {
            preparedStatement.setInt(1, accountId);
            preparedStatement.setDate(2, sqlDate);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        }
    }

    public double getTransactionSumByDate(int accountId, Date date) {
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SUM_INCOME)) {
            preparedStatement.setInt(1, accountId);
            preparedStatement.setDate(2, sqlDate);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getDouble("salary");
            }
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getMessage());
        }
        return 0;
    }

    @Override
    protected void mapToDB(Transaction transaction, PreparedStatement preparedStatement) throws SQLException {

    }

    @Override
    protected void mapToEntity(ResultSet resultSet, Transaction transaction) throws SQLException {

    }
}
