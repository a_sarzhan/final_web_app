package dao.implementation;

import dao.AbstractDao;
import entity.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountDao extends AbstractDao<Account> {
    private static final Logger LOGGER = LogManager.getLogger(AccountDao.class);
    private static final String SQL_SELECT_ACCOUNT_ID = "SELECT account_id FROM finance.accounts " +
            "WHERE user_iin = ? and card_type = ?;";

    public int findAccountIdByIIN(String iin) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ACCOUNT_ID)) {
            preparedStatement.setString(1, iin);
            preparedStatement.setString(2, "ZP_card");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("account_id");
            } else {
                return 0;
            }
        }
    }

    @Override
    protected void mapToDB(Account account, PreparedStatement preparedStatement) throws SQLException {

    }

    @Override
    protected void mapToEntity(ResultSet resultSet, Account account) throws SQLException {

    }
}
