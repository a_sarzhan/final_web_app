package dao.implementation;

import dao.AbstractDao;
import entity.Manager;
import exceptions.UserNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ManagerDao extends AbstractDao<Manager> {
    private static final Logger LOGGER = LogManager.getLogger(ManagerDao.class);
    private static final String SQL_SELECT_BY_LOGIN_PASSWORD = "SELECT manager_id, manager_login, " +
            "manager_password, manager_full_name, manager_phone, manager_email, manager_branch " +
            "FROM finance.managers WHERE manager_login = ? AND manager_password = ?;";

    public Manager findManagerByLoginPassword(String login, String password) throws UserNotFoundException, SQLException {

        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_LOGIN_PASSWORD)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Manager manager = new Manager();
                mapToEntity(resultSet, manager);
                return manager;
            } else {
                throw new UserNotFoundException("Manager verification failed! Manager not found by login: " + login);
            }
        }
    }

    @Override
    protected void mapToDB(Manager manager, PreparedStatement preparedStatement) throws SQLException {

    }

    @Override
    protected void mapToEntity(ResultSet resultSet, Manager manager) throws SQLException {
        manager.setManagerId(resultSet.getInt("manager_id"));
        manager.setManagerLogin(resultSet.getString("manager_login"));
        manager.setManagerPassword(resultSet.getString("manager_password"));
        manager.setManagerFullName(resultSet.getString("manager_full_name"));
        manager.setManagerPhone(resultSet.getString("manager_phone"));
        manager.setManagerEmail(resultSet.getString("manager_email"));
        manager.setBranch(resultSet.getString("manager_branch"));
    }
}
