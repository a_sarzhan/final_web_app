package dao;

import connection.ProxyConnection;
import entity.Entity;
import exceptions.DataNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractDao<T extends Entity> {
    protected Connection connection;
    private static final Logger LOG = LogManager.getLogger(AbstractDao.class);
    

    protected T findById(int id, String SQLStatement, Class t) throws DataNotFoundException, SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                T object = getItemInstance(t);
                mapToEntity(resultSet, object);
                return object;
            } else {
                throw new DataNotFoundException();
            }
        }
    }

    private T getItemInstance(Class t) {
        T item = null;
        try {
            item = (T) t.newInstance();
        } catch (InstantiationException | IllegalAccessException ie) {
            LOG.log(Level.ERROR, ie.getMessage());
        }
        return item;
    }

    protected boolean addToDB(T t, String sqlStatement) throws SQLException {
        try(PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            mapToDB(t, preparedStatement);
            return preparedStatement.executeUpdate() > 0;
        }
    }

    protected abstract void mapToDB(T t, PreparedStatement preparedStatement) throws SQLException;

    protected abstract void mapToEntity(ResultSet resultSet, T t) throws SQLException;

    void setConnection(ProxyConnection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }
}
