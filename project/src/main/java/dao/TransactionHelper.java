package dao;

import connection.ConnectionPool;
import connection.ProxyConnection;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class TransactionHelper {
    private static final Logger LOGGER = LogManager.getLogger(TransactionHelper.class);
    private static final ProxyConnection connection = ConnectionPool.getInstance().getConnectionFromPool();

    public void beginTransaction(AbstractDao dao, AbstractDao... daos)  {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
        }
        dao.setConnection(connection);
        for (AbstractDao thisDao : daos) {
            thisDao.setConnection(connection);
        }
    }

    public void endTransaction(AbstractDao dao, AbstractDao... daos) {
        try {
            connection.setAutoCommit(true);
            connection.close();
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
        }
    }

    public void commit() {
        try {
            connection.commit();
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
        }
    }

    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
        }
    }
}
