package exceptions;

public class TransactionException extends Exception {
    public TransactionException() {
        super("Transactions not found!");
    }

    public TransactionException(String message) {
        super(message);
    }
}
