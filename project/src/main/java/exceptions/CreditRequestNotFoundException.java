package exceptions;

public class CreditRequestNotFoundException extends Exception {
    public CreditRequestNotFoundException() {
        super("Credit Request Not Found in Database");
    }

    public CreditRequestNotFoundException(String message) {
        super(message);
    }
}
