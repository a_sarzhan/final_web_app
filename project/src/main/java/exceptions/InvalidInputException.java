package exceptions;

public class InvalidInputException extends Exception {
    public InvalidInputException() {
        super("Invalid Input Value!");
    }

    public InvalidInputException(String message) {
        super(message);
    }
}
