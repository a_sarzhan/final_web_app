package exceptions;

public class DataNotFoundException extends Exception {
    public DataNotFoundException() {
        super("Data not found in Database");
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}
