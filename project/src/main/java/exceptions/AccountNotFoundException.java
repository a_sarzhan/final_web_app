package exceptions;

public class AccountNotFoundException extends Exception {
    public AccountNotFoundException() {
        super("Account Not Found in Database");
    }

    public AccountNotFoundException(String message) {
        super(message);
    }
}
