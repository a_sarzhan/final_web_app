package localization;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceLocalizer {
    private static final Logger LOGGER = LogManager.getLogger(ResourceLocalizer.class);
    private static ResourceLocalizer localizer = new ResourceLocalizer();
    private Locale locale;
    private ResourceBundle pages;
    private ResourceBundle messages;

    private ResourceLocalizer() {
        this.locale = Locale.getDefault();
        LOGGER.log(Level.INFO, " Singleton constructor locale " + locale);
        this.pages = ResourceBundle.getBundle("page", locale, new PropertyFileEncoding());
        this.messages = ResourceBundle.getBundle("message", new PropertyFileEncoding());
    }

    public static ResourceLocalizer getInstance() {
        return localizer;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public void setMessages(ResourceBundle messages) {
        this.messages = messages;
    }

    public String getPage(String key) {
        return (String) pages.getObject(key);
    }

    public String getMessage(String key) {
        return (String) messages.getObject(key);
    }
}
