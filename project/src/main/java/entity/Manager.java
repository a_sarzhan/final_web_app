package entity;

import java.util.Objects;

public class Manager extends Entity {
    private int managerId;
    private String managerLogin;
    private String managerPassword;
    private String managerFullName;
    private String managerPhone;
    private String managerEmail;
    private String branch;

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public String getManagerLogin() {
        return managerLogin;
    }

    public void setManagerLogin(String managerLogin) {
        this.managerLogin = managerLogin;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public void setManagerPassword(String managerPassword) {
        this.managerPassword = managerPassword;
    }

    public String getManagerFullName() {
        return managerFullName;
    }

    public void setManagerFullName(String managerFullName) {
        this.managerFullName = managerFullName;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manager manager = (Manager) o;
        return managerId == manager.managerId &&
                Objects.equals(managerLogin, manager.managerLogin) &&
                Objects.equals(managerPassword, manager.managerPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(managerId, managerLogin, managerPassword);
    }

    @Override
    public String toString() {
        return  "\nmanagerId=" + managerId +
                "\nmanagerLogin='" + managerLogin +
                "\nmanagerPassword='" + managerPassword +
                "\nmanagerFullName='" + managerFullName +
                "\nmanagerPhone='" + managerPhone +
                "\nmanagerEmail='" + managerEmail +
                "\nbranch='" + branch +
                "\n---------------------------------------------------------------------------------------------------";
    }
}
