package entity;

import java.util.Date;
import java.util.Objects;

public class CreditRequest extends Entity {
    private int requestId;
    private int creditSumm;
    private int creditMonth;
    private boolean hasSalaryCard;
    private double clientSalary;
    private Date requestDate;
    private String requestStatus;
    private double rate;
    private double commission;
    private double monthlyPayment;
    private String clientDecision;
    private User requestOwner;

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getCreditSumm() {
        return creditSumm;
    }

    public void setCreditSumm(int creditSumm) {
        this.creditSumm = creditSumm;
    }

    public int getCreditMonth() {
        return creditMonth;
    }

    public void setCreditMonth(int creditMonth) {
        this.creditMonth = creditMonth;
    }

    public boolean isHasSalaryCard() {
        return hasSalaryCard;
    }

    public void setHasSalaryCard(boolean hasSalaryCard) {
        this.hasSalaryCard = hasSalaryCard;
    }

    public double getClientSalary() {
        return clientSalary;
    }

    public void setClientSalary(double clientSalary) {
        this.clientSalary = clientSalary;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public User getRequestOwner() {
        return requestOwner;
    }

    public void setRequestOwner(User requestOwner) {
        this.requestOwner = requestOwner;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public String getClientDecision() {
        return clientDecision;
    }

    public void setClientDecision(String clientDecision) {
        this.clientDecision = clientDecision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditRequest that = (CreditRequest) o;
        return requestId == that.requestId &&
                creditSumm == that.creditSumm &&
                creditMonth == that.creditMonth &&
                Objects.equals(requestOwner, that.requestOwner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, creditSumm, creditMonth, requestOwner);
    }

    @Override
    public String toString() {
        return  "\nrequestId=" + requestId +
                "\ncreditSumm=" + creditSumm +
                "\ncreditMonth=" + creditMonth +
                "\nhasSalaryCard=" + hasSalaryCard +
                "\nclientSalary=" + clientSalary +
                "\nrequestDate=" + requestDate +
                "\nrequestStatus='" + requestStatus +
                "\nrate=" + rate +
                "\ncommission=" + commission +
                "\nmonthlyPayment=" + monthlyPayment +
                "\nclientDecision='" + clientDecision +
                "\nrequestOwner=" + requestOwner +
                "\n---------------------------------------------------------------------------------------------------";
    }
}
