package entity;

import java.io.Serializable;
import java.util.Objects;

public class User extends Entity implements Serializable {
    private int userId;
    private String userLogin;
    private String userPassword;
    private String userIIN;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String userPhone;

    public User() {
    }

    public User(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserIIN() {
        return userIIN;
    }

    public void setUserIIN(String userIIN) {
        this.userIIN = userIIN;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getUserId() == user.getUserId() &&
                Objects.equals(getUserLogin(), user.getUserLogin()) &&
                Objects.equals(getUserPassword(), user.getUserPassword()) &&
                Objects.equals(getUserIIN(), user.getUserIIN());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getUserLogin(), getUserPassword(), getUserIIN());
    }

    @Override
    public String toString() {
        return "\nuserId=" + userId +
                "\nuserLogin='" + userLogin +
                "\nuserPassword='" + userPassword +
                "\nuserIIN='" + userIIN +
                "\nuserFirstName='" + userFirstName +
                "\nuserLastName='" + userLastName +
                "\nuserEmail='" + userEmail +
                "\nuserPhone='" + userPhone +
                "\n---------------------------------------------------------------------------------------------------";
    }
}
