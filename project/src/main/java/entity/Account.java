package entity;

import java.util.Date;
import java.util.Objects;

public class Account extends Entity {
    private int accountId;
    private String accountNumber;
    private String cardNumber;
    private String cardType;
    private Date startDate;
    private Date endDate;
    private String currency;
    private User accountOwner;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public User getAccountOwner() {
        return accountOwner;
    }

    public void setAccountOwner(User accountOwner) {
        this.accountOwner = accountOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountId == account.accountId &&
                accountNumber.equals(account.accountNumber) &&
                accountOwner.equals(account.accountOwner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, accountNumber, accountOwner);
    }

    @Override
    public String toString() {
        return "\naccountId=" + accountId +
                "\naccountNumber='" + accountNumber +
                "\ncardNumber='" + cardNumber +
                "\ncardType='" + cardType +
                "\nstartDate=" + startDate +
                "\nendDate=" + endDate +
                "\ncurrency='" + currency +
                "\naccountOwner=" + accountOwner +
                "\n---------------------------------------------------------------------------------------------------";
    }
}
