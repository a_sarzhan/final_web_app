package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.User;
import exceptions.DataNotFoundException;
import exceptions.UserNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.LogicPerformer;
import receivers.UserLogic;
import validation.InputValidator;

import java.sql.SQLException;

public class UpdateClientInfoCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(UpdateClientInfoCommand.class);

    public UpdateClientInfoCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("profile_redirect"));
        User userFromJsp = new User();
        userFromJsp.setUserId(Integer.parseInt(content.getRequestParameters("userId")[0]));
        userFromJsp.setUserLogin(content.getRequestParameters("userName")[0]);
        userFromJsp.setUserPassword(content.getRequestParameters("password")[0]);
        userFromJsp.setUserIIN(content.getRequestParameters("IIN")[0]);
        userFromJsp.setUserFirstName(content.getRequestParameters("firstName")[0]);
        userFromJsp.setUserLastName(content.getRequestParameters("lastName")[0]);
        userFromJsp.setUserEmail(content.getRequestParameters("email")[0]);
        userFromJsp.setUserPhone(content.getRequestParameters("phone")[0]);
        UserLogic logic = (UserLogic) performer;
        InputValidator validator = InputValidator.getValidator();
        if (!validator.isValidUserInput(userFromJsp)) {
            response.addSessionAttribute("editErrorMsg", localizer.getMessage("validation.fail"));
            LOGGER.log(Level.INFO, "User profile input Validation fail on update: User with login: " + userFromJsp.getUserLogin());
            return response;
        }
        try {
            User newUser = logic.getUpdatedUserEntity(userFromJsp);
            LOGGER.log(Level.INFO, "User personal data has been successfully updated. User id: " + newUser.getUserId());
            response.addSessionAttribute("user", newUser);
            response.addSessionAttribute("editSuccessMsg", localizer.getMessage("profile.success"));
        } catch (UserNotFoundException e) {
            LOGGER.log(Level.INFO, "User personal data fail on update. Reason: " + e.getMessage());
            response.addSessionAttribute("editErrorMsg", localizer.getMessage("profile.fail"));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " " + err.getMessage());
            response.addSessionAttribute("systemError", "User personal data fail on update. System error: " +
                    + err.getErrorCode() + " // " + err.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        response.addRequestParameter("command", "update_client_info");
        response.addParametersToPage();
        return response;
    }
}
