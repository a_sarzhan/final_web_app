package commands;

import content.RequestContent;
import content.ResponseContent;
import receivers.UserLogic;

public class LogOutCommand extends AbstractCommand {
    public LogOutCommand(UserLogic performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        ResponseContent response = new ResponseContent();
        response.setDirection(Direction.REDIRECT);
        response.invalidateSession();
        response.setPage(localizer.getPage("root"));
        return response;
    }
}
