package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import exceptions.CreditRequestNotFoundException;
import exceptions.DataNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;
import java.util.Map;

public class DeleteCreditRequestCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(DeleteCreditRequestCommand.class);

    public DeleteCreditRequestCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        CreditLogic logic = (CreditLogic) performer;
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("my_request_redirect"));
        try {
            int requestId = Integer.parseInt(content.getRequestParameters("requestId")[0]);
            int userId = Integer.parseInt(content.getRequestParameters("userId")[0]);
            if (logic.isDeletedCreditRequestById(requestId)) {
                LOGGER.log(Level.INFO, "Credit request delete operation succeeded");
                response.addSessionAttribute("deleteSuccessMsg", localizer.getMessage("profile.requestDeleteSuccess"));
                CreditRequest creditRequest = logic.findUserCreditRequest(userId);
                response.addSessionAttribute("myRequest", creditRequest);
            } else {
                LOGGER.log(Level.INFO, "Credit request delete operation failed");
                response.addSessionAttribute("deleteErrorMsg", localizer.getMessage("profile.requestDeleteFail"));
                CreditRequest creditRequest = logic.findUserCreditRequest(requestId);
                response.addSessionAttribute("myRequest", creditRequest);
            }
        } catch (NumberFormatException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            response.addSessionAttribute("deleteErrorMsg", localizer.getMessage("profile.requestDeleteFail"));
        } catch (CreditRequestNotFoundException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            response.addSessionAttribute("noRequest", localizer.getMessage("profile.requestNotFound"));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getMessage());
            response.addSessionAttribute("systemError", "Credit request delete operation fail. System error: " +
                    err.getErrorCode() + " // " + err.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        if (response.getPage().equals(localizer.getPage("my_request_redirect"))) {
            Map<String, String[]> parameters = content.getRequestParameterMap();
            parameters.forEach((key, value) -> {
                if (!key.toLowerCase().equals("command")) {
                    response.addRequestParameter(key, value[0]);
                }
            });
            response.addRequestParameter("command", localizer.getPage("my_request"));
            response.addParametersToPage();
        }
        return response;
    }
}
