package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RequestSearchCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(RequestSearchCommand.class);

    public RequestSearchCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        ResponseContent response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("credit_request"));
        CreditLogic creditPerformer = (CreditLogic) performer;
        LinkedHashMap<String, String> searchMap = new LinkedHashMap<>();
        String iin = content.getRequestParameters("IIN")[0];
        String requestDateFrom = content.getRequestParameters("requestDateFrom")[0];
        String requestDateTo = content.getRequestParameters("requestDateTo")[0];
        String requestStatus = content.getRequestParameters("requestStatus")[0];
        String clientDecision = content.getRequestParameters("clientDecision")[0];
        searchMap.put("IIN", iin);
        searchMap.put("requestDateFrom", requestDateFrom);
        searchMap.put("requestDateTo", requestDateTo);
        searchMap.put("requestStatus", requestStatus);
        searchMap.put("clientDecision", clientDecision);

        searchMap.forEach((key, value) -> {
            LOGGER.info("LOGGGE " + key + " // " + value);
        });
        try {
            ArrayList<CreditRequest> creditRequestList = creditPerformer.findCreditRequestsByParameters(searchMap);
            if (creditRequestList.isEmpty()) {
                response.addRequestAttribute("requestsNotFound", localizer.getMessage("requestSearch.notFound"));
            } else {
                int itemPerPage = 10;
                int totalPages = (int) Math.floor(creditRequestList.size() / (itemPerPage + 1)) + 1;
                int pageNo = content.isRequestParameterExist("pageNo") ?
                        Integer.parseInt(content.getRequestParameters("pageNo")[0]) : 1;
                int startIndex = (pageNo - 1) * itemPerPage;
                int endIndex = Math.min((itemPerPage * pageNo), creditRequestList.size());
                LOGGER.log(Level.INFO, "SUBLIST startIndex " + startIndex + " endIndex " + endIndex);
                List<CreditRequest> resultList = creditRequestList.subList(startIndex, endIndex);

                response.addRequestAttribute("totalPages", totalPages);
                response.addRequestAttribute("pageNo", pageNo);
                response.addRequestAttribute("iin", iin);
                response.addRequestAttribute("dateFrom", requestDateFrom);
                response.addRequestAttribute("dateTo", requestDateTo);
                response.addRequestAttribute("status", requestStatus);
                response.addRequestAttribute("decision", clientDecision);
                response.addRequestAttribute("creditRequest", resultList);
            }
        } catch (NumberFormatException | ParseException | SQLException e) {
            LOGGER.log(Level.ERROR, "Error occurred while searching requests by specified parameters. Error:" + e.getMessage());
            response.addSessionAttribute("systemError", "Request searching by parameters failed. System error: " + e.getMessage());
            response.setPage(localizer.getPage("error"));
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.ERROR, "Error occurred while searching requests by specified parameters. Error:" + e.getMessage());
            response.addSessionAttribute("systemError", "Request searching by parameters failed. System error: " + e.getMessage());
            response.setPage(localizer.getPage("error"));
        }

        return response;
    }
}
