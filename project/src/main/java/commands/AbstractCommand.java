package commands;

import content.RequestContent;
import content.ResponseContent;
import localization.ResourceLocalizer;
import receivers.LogicPerformer;

import java.util.ResourceBundle;

public abstract class AbstractCommand {
    protected LogicPerformer performer;
    protected ResponseContent response;
    protected ResourceLocalizer localizer;

    public AbstractCommand(LogicPerformer performer) {
        this.performer = performer;
        this.localizer = ResourceLocalizer.getInstance();
    }

    public abstract ResponseContent execute(RequestContent content);

}
