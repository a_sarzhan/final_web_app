package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import entity.User;
import exceptions.InvalidInputException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;
import validation.InputValidator;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

public class AddCreditRequestCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(AddCreditRequestCommand.class);

    public AddCreditRequestCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("main_redirect"));
        CreditLogic creditLogic = (CreditLogic) performer;
        CreditRequest creditRequest = null;
        try {
            creditRequest = mapRequestContentToCreditEntity(content);
            if (creditLogic.addCreditRequest(creditRequest)) {
                response.addSessionAttribute("requestSuccessMsg", localizer.getMessage("request.success"));
                LOGGER.log(Level.INFO, localizer.getMessage("request.success"));
            } else {
                response.addSessionAttribute("requestErrorMsg", localizer.getMessage("request.fail"));
                LOGGER.log(Level.INFO, localizer.getMessage("request.fail"));
            }
        } catch (InvalidInputException | NumberFormatException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            response.addSessionAttribute("requestErrorMsg", localizer.getMessage("validation.fail"));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " " + err.getMessage());
            response.addSessionAttribute("systemError", "User credit request submission fail. System error: "
                    + err.getErrorCode() + " " + err.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        if (response.getPage().equals(localizer.getPage("main_redirect"))) {
            Map<String, String[]> parameters = content.getRequestParameterMap();
            parameters.forEach((key, value) -> {
                if (!key.toLowerCase().equals("command")) {
                    response.addRequestParameter(key, value[0]);
                }
            });
            response.addRequestParameter("command", localizer.getPage("main"));
            response.addParametersToPage();
        }

        return response;
    }

    private CreditRequest mapRequestContentToCreditEntity(RequestContent content) throws InvalidInputException,
            NumberFormatException {
        CreditRequest creditRequest = new CreditRequest();
        boolean hasCard;
        int userId = Integer.parseInt(content.getRequestParameters("userId")[0]);
        int creditSum = Integer.parseInt(content.getRequestParameters("creditAmount")[0]);
        int period = Integer.parseInt(content.getRequestParameters("creditPeriod")[0]);
        double salary = Double.parseDouble(content.getRequestParameters("clientSalary")[0]);
        String hasCardString = content.getRequestParameters("clientMFCSalary")[0];
        LOGGER.log(Level.INFO, "Credit request input values. sum: " + creditSum + " period:  " + period + " salary: "
                + hasCardString + " has MFC card: " + hasCardString);
        if (hasCardString != null && !hasCardString.isEmpty()) {
            hasCard = Boolean.parseBoolean(hasCardString);
        } else {
            LOGGER.log(Level.INFO, "Credit request input value for MFC card is EMPTY");
            throw new InvalidInputException("<Has user MFC salary card> required input field is empty");
        }

        InputValidator validator = InputValidator.getValidator();
        if (validator.isValidCreditRequestFields(creditSum, period, salary)) {
            creditRequest.setCreditSumm(creditSum);
            creditRequest.setCreditMonth(period);
            creditRequest.setClientSalary(salary);
            creditRequest.setHasSalaryCard(hasCard);
            creditRequest.setRequestStatus("active");
            creditRequest.setRequestDate(new Date());
            creditRequest.setRequestOwner(new User(userId));
        } else {
            throw new InvalidInputException("Invalid value of credit sum/ credit period/ salary");
        }
        return creditRequest;
    }
}
