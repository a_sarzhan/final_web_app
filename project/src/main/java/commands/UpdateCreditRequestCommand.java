package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import exceptions.DataNotFoundException;
import exceptions.InvalidInputException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;
import validation.InputValidator;

import java.sql.SQLException;
import java.util.Map;

public class UpdateCreditRequestCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(UpdateCreditRequestCommand.class);

    public UpdateCreditRequestCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        CreditLogic logic = (CreditLogic) performer;
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("my_request_redirect"));
        try {
            CreditRequest creditRequest = mapContentToCreditEntity(content);
            CreditRequest resultRequest = logic.getUpdatedCreditRequest(creditRequest);
            response.addSessionAttribute("myRequest", resultRequest);
            response.addSessionAttribute("updateSuccessMsg", localizer.getMessage("profile.requestUpdateSuccess"));
            LOGGER.log(Level.INFO, "Credit Request parameters successfully updated by Client!");
        } catch (InvalidInputException | DataNotFoundException | NumberFormatException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            response.addSessionAttribute("updateErrorMsg", localizer.getMessage("profile.requestUpdateFail"));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getMessage());
            response.addSessionAttribute("systemError", "Credit request update fail. System error: " + err.getErrorCode()
                    + " // " + err.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        if (response.getPage().equals(localizer.getPage("my_request_redirect"))) {
            Map<String, String[]> parameters = content.getRequestParameterMap();
            parameters.forEach((key, value) -> {
                if (!key.toLowerCase().equals("command")) {
                    response.addRequestParameter(key, value[0]);
                }
            });
            response.addRequestParameter("command", localizer.getPage("my_request"));
            response.addParametersToPage();
        }
        return response;
    }

    private CreditRequest mapContentToCreditEntity(RequestContent content) throws InvalidInputException, NumberFormatException {
        CreditRequest creditRequest = new CreditRequest();
        boolean hasCard;
        int requestId = Integer.parseInt(content.getRequestParameters("requestId")[0]);
        int creditSum = Integer.parseInt(content.getRequestParameters("creditAmount")[0]);
        int period = Integer.parseInt(content.getRequestParameters("creditPeriod")[0]);
        double salary = Double.parseDouble(content.getRequestParameters("clientSalary")[0]);
        String hasCardString = content.getRequestParameters("clientMFCSalary")[0];
        LOGGER.log(Level.INFO, "Update credit request details. Sum: " + creditSum + " period: " + period + " salary: "
                + salary + " has Card: " + hasCardString);
        if (hasCardString != null && !hasCardString.isEmpty()) {
            hasCard = Boolean.parseBoolean(hasCardString);
        } else {
            LOGGER.log(Level.INFO, "!!!!!HAS CARD IS EMPTY IN UPDATE!!!!!");
            throw new InvalidInputException("<Has user MFC salary card> required input field is empty");
        }

        InputValidator validator = InputValidator.getValidator();
        if (validator.isValidCreditRequestFields(creditSum, period, salary)) {
            creditRequest.setRequestId(requestId);
            creditRequest.setCreditSumm(creditSum);
            creditRequest.setCreditMonth(period);
            creditRequest.setClientSalary(salary);
            creditRequest.setHasSalaryCard(hasCard);
        } else {
            throw new InvalidInputException("Invalid value of credit sum/ credit period/ salary");
        }
        return creditRequest;
    }
}
