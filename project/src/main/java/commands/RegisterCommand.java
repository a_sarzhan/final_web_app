package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.User;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.LogicPerformer;
import receivers.UserLogic;
import validation.InputValidator;

import java.sql.SQLException;

public class RegisterCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(RegisterCommand.class);

    RegisterCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("register_redirect"));
        UserLogic userLogic = (UserLogic) performer;
        User user = new User();
        user.setUserLogin(content.getRequestParameters("userName")[0]);
        user.setUserPassword(content.getRequestParameters("password")[0]);
        user.setUserIIN(content.getRequestParameters("IIN")[0]);
        user.setUserFirstName(content.getRequestParameters("firstName")[0]);
        user.setUserLastName(content.getRequestParameters("lastName")[0]);
        user.setUserEmail(content.getRequestParameters("email")[0]);
        user.setUserPhone(content.getRequestParameters("phone")[0]);
        InputValidator validator = InputValidator.getValidator();
        if (!validator.isValidUserInput(user)) {
            response.addSessionAttribute("registerErrorMsg", localizer.getMessage("validation.fail"));
            return response;
        }
        try {
            if (userLogic.getUserIdByLogin(user.getUserLogin()) > 0) {
                LOGGER.log(Level.INFO, "Registration fail: User with login: " + user.getUserLogin() + " already exist");
                response.addSessionAttribute("registerErrorMsg", localizer.getMessage("register.userExistError"));
            } else {
                if (userLogic.addNewUser(user)) {
                    response.addSessionAttribute("registerSuccessMsg", localizer.getMessage("register.success"));
                    LOGGER.log(Level.INFO, "Registration success: User with login: " + user.getUserLogin());
                } else {
                    response.addSessionAttribute("registerErrorMsg", localizer.getMessage("register.fail"));
                    LOGGER.log(Level.INFO, "Registration fail: " + localizer.getMessage("register.fail"));
                }
            }
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " " + err.getMessage());
            response.addSessionAttribute("systemError", "User registration fail. System error: " + err.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        response.addRequestParameter("command", "register");
        response.addParametersToPage();
        return response;
    }
}
