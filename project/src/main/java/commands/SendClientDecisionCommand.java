package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import exceptions.CreditRequestNotFoundException;
import exceptions.DataNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;
import java.util.Map;

public class SendClientDecisionCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(SendClientDecisionCommand.class);
    public SendClientDecisionCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        content.getRequest().removeAttribute("noRequest");
        content.getRequest().removeAttribute("myRequest");
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("my_request_redirect"));
        LOGGER.info(content.getRequestParameters("requestId")[0]);
        LOGGER.info(content.getRequestParameters("clientDecision")[0]);
        int requestId = Integer.parseInt(content.getRequestParameters("requestId")[0]);
        String decision = content.getRequestParameters("clientDecision")[0];
        CreditLogic logic = (CreditLogic) performer;
        try {
            CreditRequest creditRequest = logic.getUpdatedRequestWithDecision(requestId, decision);
            response.addSessionAttribute("myRequest", creditRequest);
            response.addSessionAttribute("decisionFeedback", localizer.getMessage("profile.decisionSuccess"));
            LOGGER.log(Level.INFO, "Credit request client decision was sent and updated in DB");
        } catch (CreditRequestNotFoundException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            response.addSessionAttribute("noRequest", localizer.getMessage("profile.requestNotFound"));
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            response.addSessionAttribute("systemError", "Credit request update client decision fail. System error: " +
                    e.getErrorCode() + " // " + e.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        if (response.getPage().equals(localizer.getPage("my_request_redirect"))) {
            Map<String, String[]> parameters = content.getRequestParameterMap();
            parameters.forEach((key, value) -> {
                if (!key.toLowerCase().equals("command")) {
                    response.addRequestParameter(key, value[0]);
                }
            });
            response.addRequestParameter("command", localizer.getPage("my_request"));
            response.addParametersToPage();
        }
        return response;

    }
}
