package commands.mini_commands;

import commands.AbstractCommand;
import commands.Direction;
import content.RequestContent;
import content.ResponseContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.LogicPerformer;

public class OpenProfileCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(OpenProfileCommand.class);

    public OpenProfileCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("profile"));
        return response;
    }
}
