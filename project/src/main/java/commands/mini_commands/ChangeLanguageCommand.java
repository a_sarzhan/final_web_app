package commands.mini_commands;

import commands.AbstractCommand;
import commands.Direction;
import content.RequestContent;
import content.ResponseContent;
import localization.PropertyFileEncoding;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.LogicPerformer;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.util.Locale;
import java.util.ResourceBundle;

public class ChangeLanguageCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(ChangeLanguageCommand.class);

    public ChangeLanguageCommand(LogicPerformer performer) {
        super(performer);
        LOGGER.log(Level.INFO, "call from changeLang constructor");
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        LOGGER.log(Level.INFO, "in ChangeLanguageCommand execute");
        String language = content.getRequestParameters("lang")[0];
        LOGGER.log(Level.INFO, " language = " + language);
        Locale locale;
        switch (language) {
            case "en":
                locale = new Locale("en", "US");
                break;
            case "ru":
                locale = new Locale("ru", "KZ");
                break;
            default:
                locale = content.getRequest().getLocale();
        }
        LOGGER.log(Level.INFO, "added locale to sess " + locale);
        resourceBundleEncoding(content.getRequest().getSession(), locale);
        response.addSessionAttribute("locale", locale);
        //response.addRequestAttribute("errorLoginPassMessage", localizer.getMessage("error.login"));
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("main"));
        Config.set(content.getRequest().getSession(), Config.FMT_LOCALE, locale);
        return response;
    }

    private void resourceBundleEncoding(HttpSession session, Locale locale) {
        String propertyFiles[] = {"label", "message", "admin"};
        ResourceBundle bundle;
        LocalizationContext localizationContext;
        for (String property : propertyFiles) {
            bundle = ResourceBundle.getBundle(property, locale, new PropertyFileEncoding());
            localizationContext = new LocalizationContext(bundle, locale);
            Config.set(session, Config.FMT_LOCALIZATION_CONTEXT, localizationContext);
            if (property.equals("message")) {
                localizer.setMessages(bundle);
            }
        }
    }
}
