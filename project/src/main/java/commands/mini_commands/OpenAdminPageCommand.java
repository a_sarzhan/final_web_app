package commands.mini_commands;

import commands.AbstractCommand;
import commands.Direction;
import content.RequestContent;
import content.ResponseContent;
import receivers.LogicPerformer;

public class OpenAdminPageCommand extends AbstractCommand {
    public OpenAdminPageCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("admin"));
        return response;
    }
}
