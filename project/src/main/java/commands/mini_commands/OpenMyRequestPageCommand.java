package commands.mini_commands;

import commands.AbstractCommand;
import commands.Direction;
import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import exceptions.CreditRequestNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;

public class OpenMyRequestPageCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(OpenMyRequestPageCommand.class);

    public OpenMyRequestPageCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        CreditLogic logic = (CreditLogic) performer;
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("my_request"));

        int userId = Integer.parseInt(content.getRequestParameters("userId")[0]);
        try {
            CreditRequest creditRequest = logic.findUserCreditRequest(userId);
            response.addRequestAttribute("myRequest", creditRequest);
        } catch (CreditRequestNotFoundException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            response.addRequestAttribute("noRequest", localizer.getMessage("profile.requestNotFound"));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
            response.addRequestAttribute("systemError", "Error occurred while getting users last credit request." +
                    " System error: " + err.getErrorCode() + " //" + err.getMessage());
            response.setPage(localizer.getPage("error"));
        }

        return response;
    }
}
