package commands.mini_commands;

import commands.AbstractCommand;
import commands.Direction;
import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OpenRequestProcessPageCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(OpenRequestProcessPageCommand.class);
    public OpenRequestProcessPageCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("request_process"));
        CreditLogic creditPerformer = (CreditLogic) performer;
        try {
            ArrayList<CreditRequest> activeRequestList = creditPerformer.findActiveCreditRequests();

            if (activeRequestList.isEmpty()) {
                response.addRequestAttribute("activeRequestNotFound", localizer.getMessage("activeRequest.notFound"));
                LOGGER.log(Level.INFO, localizer.getMessage("activeRequest.notFound"));
            } else {
                int itemPerPage = 4;
                int totalPages = (int) Math.floor(activeRequestList.size() / (itemPerPage + 1)) + 1;
                int pageNo = content.isRequestParameterExist("pageNo") ?
                        Integer.parseInt(content.getRequestParameters("pageNo")[0]) : 1;
                int startIndex = (pageNo - 1) * itemPerPage;
                int endIndex = Math.min((itemPerPage * pageNo), activeRequestList.size());
                LOGGER.info("MANAGER OPEN ACTIVE REQUESTS - > pageNo " + pageNo + " totalPages " + totalPages);
                List<CreditRequest> resultList = activeRequestList.subList(startIndex, endIndex);
                response.addSessionAttribute("inTerms", null);
                response.addSessionAttribute("activeRequest", null);
                response.addRequestAttribute("totalPages", totalPages);
                response.addRequestAttribute("pageNo", pageNo);
                response.addRequestAttribute("activeRequest", resultList);
                response.addSessionAttribute("inTerms", null);
            }
        } catch (NumberFormatException | SQLException err) {
            LOGGER.log(Level.ERROR, "Error occurred while opening active requests page. Error:" + err.getMessage());
            response.addSessionAttribute("systemError", "Opening manager's active requests page failed. System error: " + err.getMessage());
            response.setPage(localizer.getPage("error"));
        }
        return response;
    }
}
