package commands.mini_commands;

import commands.AbstractCommand;
import commands.Direction;
import content.RequestContent;
import content.ResponseContent;
import receivers.LogicPerformer;

public class OpenCreditRequestPageCommand extends AbstractCommand {
    public OpenCreditRequestPageCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        response.addRequestAttribute("pageNo", 1);
        response.setPage(localizer.getPage("credit_request"));
        return response;
    }
}
