package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import exceptions.CreditRequestNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;
import receivers.UserLogic;

import java.sql.SQLException;

public class OpenProfileCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(OpenProfileCommand.class);

    public OpenProfileCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        LOGGER.log(Level.INFO, "<<<<<<<<<<<<<<<<<  Open Profile >>>>>>>>> ");
        response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        response.setPage(localizer.getPage("profile"));
        return response;
    }
}
