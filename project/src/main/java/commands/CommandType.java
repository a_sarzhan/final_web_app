package commands;

import commands.mini_commands.*;
import receivers.CreditLogic;
import receivers.UserLogic;

public enum CommandType {
        LOGIN(new LogInCommand(null)),
        REGISTER(new RegisterCommand(new UserLogic())),
        LOGOUT(new LogOutCommand(new UserLogic())),
        OPEN_REGISTER_PAGE(new OpenRegisterPageCommand(null)),
        OPEN_LOGIN_PAGE(new OpenLoginPageCommand(null)),
        OPEN_MAIN_PAGE(new OpenMainPageCommand(null)),
        OPEN_ADMIN_PAGE(new OpenAdminPageCommand(null)),
        OPEN_REQUEST_PAGE(new OpenCreditRequestPageCommand(null)),
        OPEN_REQUEST_PROCESS_PAGE(new OpenRequestProcessPageCommand(new CreditLogic())),
        CHANGE_LANGUAGE(new ChangeLanguageCommand(null)),
        CREDIT_REQUEST(new AddCreditRequestCommand(new CreditLogic())),
        REQUEST_SEARCH(new RequestSearchCommand(new CreditLogic())),
        PROCESS_REQUEST(new ProcessActiveRequestCommand(new CreditLogic())),
        OPEN_PROFILE(new OpenProfileCommand(null)),
        SEND_DECISION(new SendClientDecisionCommand(new CreditLogic())),
        UPDATE_CLIENT_INFO(new UpdateClientInfoCommand(new UserLogic())),
        UPDATE_CREDIT_REQUEST(new UpdateCreditRequestCommand(new CreditLogic())),
        DELETE_CREDIT_REQUEST(new DeleteCreditRequestCommand(new CreditLogic())),
        OPEN_MY_REQUEST(new OpenMyRequestPageCommand(new CreditLogic()));

        private AbstractCommand command;

        CommandType(AbstractCommand command) {
            this.command = command;
        }

        public AbstractCommand getCommand() {
            return command;
        }
}
