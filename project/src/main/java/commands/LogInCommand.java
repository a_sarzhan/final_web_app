package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.Manager;
import entity.User;
import exceptions.UserNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.ManagerLogic;
import receivers.UserLogic;

import java.sql.SQLException;


public class LogInCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(LogInCommand.class);

    public LogInCommand(UserLogic performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.FORWARD);
        String userRole = content.getRequestParameters("userRole")[0];
        try {
            if (userRole.equals("client")) {
                performer = new UserLogic();
                User user = ((UserLogic) performer).verifyUser(content);
                LOGGER.log(Level.INFO, "User login validation success. User login: " + user.getUserLogin());
                response.addSessionAttribute("user", user);
                response.setPage(localizer.getPage("main"));
            }
            if (userRole.equals("manager")) {
                performer = new ManagerLogic();
                Manager manager = ((ManagerLogic) performer).verifyManager(content);
                LOGGER.log(Level.INFO, "Manager login validation success. User login: " + manager.getManagerLogin());
                response.addSessionAttribute("manager", manager);
                response.setPage(localizer.getPage("admin"));
            }
        } catch (UserNotFoundException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            if (userRole.equals("client")) {
                response.addRequestAttribute("errorUserLoginMsg", localizer.getMessage("login.error"));
            }
            if (userRole.equals("manager")) {
                response.addRequestAttribute("errorManagerLoginMsg", localizer.getMessage("login.adminError"));
            }
            response.setPage(localizer.getPage("login"));
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getMessage());
            response.addRequestAttribute("systemError", "User Login fail. System error: " + err.getMessage());
            response.setPage(localizer.getPage("error"));
        }
        return response;
    }
}
