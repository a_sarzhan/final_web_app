package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import entity.User;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;
import java.util.Date;

public class CreditRequestCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(CreditRequestCommand.class);

    public CreditRequestCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        LOGGER.log(Level.INFO, "in CreditRequestCommand");
        response.setDirection(Direction.FORWARD);   // check redirect as well
        response.setPage(localizer.getPage("main"));
        CreditLogic creditLogic = (CreditLogic) performer;
        User user = (User) content.getRequest().getSession().getAttribute("user");
        LOGGER.log(Level.INFO, "user name is " + user.getUserFirstName());
        CreditRequest creditRequest = new CreditRequest();
        creditRequest.setCreditSumm(Integer.parseInt(content.getRequestParameters("creditAmount")[0]));
        creditRequest.setCreditMonth(Integer.parseInt(content.getRequestParameters("creditPeriod")[0]));
        creditRequest.setClientSalary(Double.parseDouble(content.getRequestParameters("clientSalary")[0]));
        creditRequest.setHasSalaryCard(Boolean.parseBoolean(content.getRequestParameters("clientMFCSalary")[0]));
        creditRequest.setRequestOwner(user);
        creditRequest.setRequestStatus("active");
        creditRequest.setRequestDate(new Date());
        //go to logic block
        try {
            if (creditLogic.addCreditRequest(creditRequest)) {
                LOGGER.log(Level.INFO, "is request entity updated? " + creditRequest.getRequestId());
                response.addRequestAttribute("creditRequest", creditRequest);
                response.addRequestAttribute("requestSuccessMsg", localizer.getMessage("request.success"));
                response.addRequestAttribute("requestErrorMsg", null);
            } else {
                LOGGER.log(Level.INFO, "request was failed ");
                response.addRequestAttribute("requestSuccessMsg", null);
                response.addRequestAttribute("requestErrorMsg", localizer.getMessage("request.fail"));
            }
        } catch (SQLException err) {
            LOGGER.log(Level.ERROR, err.getErrorCode() + " // " + err.getMessage());
            response.addRequestAttribute("systemError", "Error occurred while adding new credit request." +
                    " System error: " + err.getErrorCode() + " //" + err.getMessage());
            response.setPage(localizer.getPage("error"));
        }
        return response;
    }
}
