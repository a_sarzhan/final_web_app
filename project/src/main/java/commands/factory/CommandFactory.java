package commands.factory;

import commands.AbstractCommand;
import commands.CommandType;
import content.RequestContent;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandFactory {
    private static final Logger LOGGER = LogManager.getLogger(CommandFactory.class);
    private CommandFactory() {
    }

    public static AbstractCommand defineCommand(RequestContent content) {
        AbstractCommand command = null;
        try {
            String commandName = content.getRequestParameters("command")[0];
            LOGGER.log(Level.INFO, commandName);
            command = CommandType.valueOf(commandName.toUpperCase()).getCommand();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return command;
    }
}
