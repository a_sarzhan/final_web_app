package commands;

import content.RequestContent;
import content.ResponseContent;
import entity.CreditRequest;
import exceptions.DataNotFoundException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import receivers.CreditLogic;
import receivers.LogicPerformer;

import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Map;

public class ProcessActiveRequestCommand extends AbstractCommand {
    private static final Logger LOGGER = LogManager.getLogger(ProcessActiveRequestCommand.class);

    public ProcessActiveRequestCommand(LogicPerformer performer) {
        super(performer);
    }

    @Override
    public ResponseContent execute(RequestContent content) {
        response = new ResponseContent();
        response.setDirection(Direction.REDIRECT);
        response.setPage(localizer.getPage("request_redirect"));
        Map<String, String[]> parameters = content.getRequestParameterMap();
        LinkedHashSet<CreditRequest> requests = null;
        String[] requestIdArray = content.getRequestParameters("creditList")[0].split(",");
        CreditLogic creditLogic = (CreditLogic) performer;
        try {
            requests = creditLogic.getProcessingCreditRequests(requestIdArray);
            requests = creditLogic.getCreditTermsOfNewRequests(requests, response);
        } catch (NumberFormatException | DataNotFoundException | SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            response.addSessionAttribute("systemError", "Error occurred while getting credit terms for client. System error: " + e.getMessage());
            response.setPage(localizer.getPage("error_redirect"));
        }
        response.addSessionAttribute("activeRequest", requests);
        response.addSessionAttribute("inTerms", "true");

        if (response.getPage().equals(localizer.getPage("request_redirect"))) {
            parameters.forEach((key, value) -> {
                if (!key.toLowerCase().equals("command"))
                    response.addRequestParameter(key, value[0]);
            });
        }
        response.addRequestParameter("command", "request_process");
        response.addParametersToPage();
        return response;
    }
}
