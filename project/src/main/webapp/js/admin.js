var menuButtons = document.querySelectorAll(".mainTab .menu button");
var menuTabs = document.querySelectorAll(".mainTab .operation");

function showTab(panelIndex, colorCode) {
    menuButtons.forEach(function(node){
        node.style.backgroundColor = "";
        node.style.color = "";
    });
    menuButtons[panelIndex].style.backgroundColor = "#666";
    menuButtons[panelIndex].style.color = colorCode;
    menuTabs.forEach(function(node){
        node.style.display = "none";
    });
    menuTabs[panelIndex].style.display = "block";
    menuTabs[panelIndex].style.backgroundColor = colorCode;
    menuTabs[panelIndex].style.color = "#666";
}
showTab(0, '#B0E0E6');