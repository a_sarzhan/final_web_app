//alert(111);
var myPass = document.getElementById("psw");
var myIIN = document.getElementById("iin");
var myPhone = document.getElementById("phone");
var myEmail = document.getElementById("email");
var myLogin = document.getElementById("userName");
var errText = pass.error.startText;
var lowerErr = "", upperErr = "", numberErr = "", sizeErr = "";
//patterns
var lowerCaseLetters = /[a-z]/g;
var upperCaseLetters = /[A-Z]/g;
var numbers = /[0-9]/g;
var passPattern =/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{8,}$/;

myPass.onkeyup = function() {
    // Validate lowercase letters
    if(!myPass.value.match(lowerCaseLetters)){
        lowerErr = " " + pass.error.lowerCase;
    } else {
        lowerErr = "";
    }
    if(!myPass.value.match(upperCaseLetters)){
        upperErr = " " + pass.error.upperCase;
    } else {
        upperErr = "";
    }
    if(!myPass.value.match(numbers)){
        numberErr = " " + pass.error.number;
    } else {
        numberErr = "";
    }
    if(myPass.value.length < 8){
        sizeErr = " " + pass.error.size;
    } else {
        sizeErr = "";
    }
    //alert(lowerErr.length);
}

function verifyPass(){
    var initErrText = errText;
    var newErr;
    if(lowerErr.length > 0){
        errText += lowerErr+ ",";
    }
    if(upperErr.length > 0){
        errText += upperErr+ ",";
    }
    if(numberErr.length > 0){
        errText += numberErr+ ",";
    }
    if(sizeErr.length > 0) {
        errText += sizeErr+ ",";
    }
    if(errText != initErrText) {
        newErr = errText.substr(0, errText.length - 1);
        document.getElementById("passError").textContent = newErr;
        return false;
    } else {
        if(!passPattern.test(myPass.value)) {
            document.getElementById("passError").textContent =
            errText + " " + pass.error.lowerCase + ", " + pass.error.upperCase + ", " + pass.error.size;
            return false;
        }
        return true;
    }
}

function verifyIIN() {
    var pattern = /^[1-9][0-9]{11}$/;
    if(!pattern.test(myIIN.value)) {
        document.getElementById("iinError").textContent = iin.error.format;
    }
    return pattern.test(myIIN.value);
}

function verifyPhone() {
    var pattern = /^[\+][7]{2}[0-9]{9}$/;
    if(!myPhone.value.match(pattern)) {
        document.getElementById("phoneError").textContent = phone.error.format;
        return false;
    } else {
        return true;
    }
}

function verifyEmail() {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(myEmail.value)){
        document.getElementById("emailError").textContent = email.error.format;
    }
    return re.test(myEmail.value);
}

function verifyLogin() {
    var pattern = /^[0-9a-zA-Z_.-]+$/;
    if(!pattern.test(myLogin.value)){
        document.getElementById("loginError").textContent = login.error.format;
    }
    return pattern.test(myLogin.value);
}

function verifyInput() {
    myPass = document.getElementById("psw");
    myIIN = document.getElementById("iin");
    myPhone = document.getElementById("phone");
    myEmail = document.getElementById("email");
    myLogin = document.getElementById("userName");
    if (verifyLogin()) {
        document.getElementById("loginError").textContent = '';
    }
    if(!verifyPass()) {
        errText = pass.error.startText;
    } else {
        document.getElementById("passError").textContent = '';
    }
    if (verifyIIN()) {
        document.getElementById("iinError").textContent = '';
    }

    if (verifyName()) {
        document.getElementById("nameError").textContent = '';
    }
    if (verifyLastName()) {
        document.getElementById("lastNameError").textContent = '';
    }

    if (verifyEmail()) {
        document.getElementById("emailError").textContent = '';
    }

    if (verifyPhone()) {
        document.getElementById("phoneError").textContent = '';
    }

    if (!verifyPass() ||  !verifyIIN() || !verifyPhone() || !verifyEmail()
    || !verifyLogin() || !verifyName() || !verifyLastName()){
        return false;
    } else {
        return true;
    }
}

