<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<c:set value="${sessionScope.get(\"user\")}" var="user" scope="page" />
<c:set value="${sessionScope.get(\"myRequest\")}" var="myRequest" scope="page" />
<fmt:setLocale value="${sessionScope.get(\"locale\")}"/>
<fmt:setBundle basename="label" var="label"/>
<fmt:setBundle basename="admin" var="admin"/>
<fmt:setBundle basename="message" var="msg"/>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="profile.title" bundle="${label}"/></title>
</head>
<style>
    <c:import url="/css/app_style.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/client_header.jsp" charEncoding = "utf-8"/>
    </header>
    <h2 style="color: #666; text-align:center;"><fmt:message key="main.welcome" bundle="${label}"/> ${user.userFirstName} ${user.userLastName}!!!</h2>
    <div class="profReqContent">
        <form name="editProfile" method="POST" action="${root}/controller" onsubmit="return verifyInput();">
        <input type="hidden" name="command" value="update_client_info"/>
        <input type="hidden" name="userId" value="${user.userId}"/>
        <input type="hidden" name="profileParam" value="${profileParam}"/>
            <div class="splitter" onchange="profileEdit();">
                <div class="div_column">
                    <fmt:message key="register.userName" bundle="${label}"/>
                    <input type="text" name="userName" id="userName" size="36" class="input" required readOnly/><br/>
                    <span id="loginError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.password" bundle="${label}"/>
                    <input type="password" name="password" id="psw" size="36" class="input" required readOnly/><br/>
                    <span id="passError" style="color: red; font-size: 12px;"></span></br>
                </div>
                <div class="div_column">
                    <fmt:message key="register.IIN" bundle="${label}" />
                    <input type="text" name="IIN" id="iin" size="36" class="input" required readOnly/><br/>
                    <span id="iinError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.firstName" bundle="${label}" />
                    <input type="text" name="firstName" id="firstName" size="36" class="input" required readOnly/><br/>
                    <span id="nameError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.lastName" bundle="${label}"/>
                    <input type="text" name="lastName" id="lastName" size="36" class="input" required readOnly/><br/>
                    <span id="lastNameError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.email" bundle="${label}"/>
                    <input type="text" name="email" id="email" size="36" class="input" required readOnly/><br/>
                    <span id="emailError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.phone" bundle="${label}"/>
                    <input type="text" name="phone" id="phone" size="36" class="input" required readOnly/><br/>
                    <span id="phoneError" style="color: red; font-size: 12px;"></span></br>
                </div>
            </div>
            <div class="buttons">
                <span class="message">${editErrorMsg}</span>
                <span class="message">${editSuccessMsg}</span></br></br>
                <c:remove var="editErrorMsg" scope="session"/>
                <c:remove var="editSuccessMsg" scope="session"/>
                <button onclick="makeEditable(); return false;" class="submit"><fmt:message key="profile.edit" bundle="${label}" /></button>
                <input id="editSubmit" type="submit" value="<fmt:message key="profile.submit" bundle="${label}"/>" class="submit"/>
                </br></br>
            </div>
        </form>
    </div>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>
    <script type="text/javascript" charset="utf-8">
        window.onload = function() {
            document.getElementsByName("userName")[0].value = "${user.userLogin}";
            document.getElementsByName("password")[0].value = "${user.userPassword}";
            document.getElementsByName("IIN")[0].value = "${user.userIIN}";
            document.getElementsByName("firstName")[0].value = "${user.userFirstName}";
            document.getElementsByName("lastName")[0].value = "${user.userLastName}";
            document.getElementsByName("email")[0].value = "${user.userEmail}";
            document.getElementsByName("phone")[0].value = "${user.userPhone}";
            document.getElementById("editSubmit").hidden = true;
        }

        function makeEditable() {
            document.getElementsByName("userName")[0].readOnly = false;
            document.getElementsByName("password")[0].readOnly = false;
            document.getElementsByName("IIN")[0].readOnly = false;
            document.getElementsByName("firstName")[0].readOnly = false;
            document.getElementsByName("lastName")[0].readOnly = false;
            document.getElementsByName("email")[0].readOnly = false;
            document.getElementsByName("phone")[0].readOnly = false;
        }

        function profileEdit() {
            document.getElementById("editSubmit").hidden = false;
        }

        var pass = pass || {};
        pass.error = {
            startText: "<fmt:message key="verify.passText" bundle="${msg}"/>",
            lowerCase: "<fmt:message key="verify.passLowerCase" bundle="${msg}"/>",
            upperCase: "<fmt:message key="verify.passUpperCase" bundle="${msg}"/>",
            number: "<fmt:message key="verify.passNumber" bundle="${msg}"/>",
            size: "<fmt:message key="verify.passSize" bundle="${msg}"/>"
        };
        var iin = iin || {};
        iin.error = { format: "<fmt:message key="verify.iinFormat" bundle="${msg}"/>"};
        var phone = phone || {};
        phone.error = { format: "<fmt:message key="verify.phoneFormat" bundle="${msg}"/>"};
        var email = email || {};
        email.error = { format: "<fmt:message key="verify.email" bundle="${msg}"/>"};
        var login = login || {};
        login.error = { format: "<fmt:message key="verify.login" bundle="${msg}"/>"};

        function verifyName() {
            var pattern = /^[A-Za-z\u0400-\u04FF]+$/;
            var myName = document.getElementById("firstName");
            if(!pattern.test(myName.value)){
                document.getElementById("nameError").textContent = "<fmt:message key="verify.name" bundle="${msg}"/>";
            }
            return pattern.test(myName.value);
        }
        function verifyLastName() {
            var pattern = /^[A-Za-z\u0400-\u04FF]+$/;
            var myLastName = document.getElementById("lastName");
            if(!pattern.test(myLastName.value)){
                document.getElementById("lastNameError").textContent = "<fmt:message key="verify.lastName" bundle="${msg}"/>";
            }
            return pattern.test(myLastName.value);
        }
    </script>
    <script src="${root}/js/validateInput.js" type="text/javascript"></script>
</body>
</html>