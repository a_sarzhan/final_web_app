<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"dossier\")}" var="dossier" scope="session"/>
<fmt:setBundle basename="admin" var="admin"/>
<html>
<head>
</head>
<body>
    <div class="dossier_class">
        <div class="search_block">
            <fmt:message key="search.IIN" bundle="${admin}" /><br/>
            <input type="text" name="IIN" size="25" class="input"/><br/><br/>
            <fmt:message key="search.ID" bundle="${admin}" /><br/>
            <input type="text" name="ID" size="25" class="input"/><br/><br/>
            <fmt:message key="search.name" bundle="${admin}"/><br/>
            <input type="text" name="name" size="25" class="input"/><br/><br/>
            <fmt:message key="search.lastName" bundle="${admin}"/><br/>
            <input type="text" name="lastName" size="25" class="input"/><br/><br/>
            <div class="submit_div">
                <input type="submit" value="<fmt:message key="dossier.searchBtn" bundle="${admin}"/>" class="submit"/></br>
            </div>
        </div>
        <c:if test="${empty dossier}">
            <div class="not_found">
                Client dossier not found! <br> <br>
                <input type="submit" value="<fmt:message key="dossier.createBtn" bundle="${admin}"/>" class="bigSubmit"/>
            </div>

        </c:if>
        <c:if test="${not empty dossier}">
            <div class="dossier_data">
                part1
            </div>
            <div class="dossier_data">
                part2
            </div>
        </c:if>

    </div>
</body>
</html>