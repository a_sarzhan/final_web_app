<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<fmt:setBundle basename="message" var="msg"/>
<html>
<head>
<meta charset='utf-8'>
    <title><fmt:message key="register.title" bundle="${label}"/></title>
</head>
<style>
    <c:import url="/css/app_style.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/header.jsp" charEncoding = "utf-8"/>
    </header>
    <h1><fmt:message key="register.title" bundle="${label}"/></h1>
    <div class="register_div">
        <form name="registerForm" method="POST" action="${root}/controller" onsubmit="return verifyInput()">
            <input type="hidden" name="command" value="register" />
            <div class="splitter">
                <div class="div_column">
                    <fmt:message key="register.userName" bundle="${label}"/>
                    <input type="text" name="userName" id="userName" size="36" class="input" required/><br/>
                    <span id="loginError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.password" bundle="${label}"/>
                    <input type="password" name="password" id="psw" size="36" class="input" required/><br/>
                    <span id="passError" style="color: red; font-size: 12px;"></span></br>
                </div>
                <div class="div_column">
                    <fmt:message key="register.IIN" bundle="${label}" />
                    <input type="text" name="IIN" id="iin" size="36" class="input" required/><br/>
                    <span id="iinError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.firstName" bundle="${label}" />
                    <input type="text" name="firstName" id="firstName" size="36" class="input" required/><br/>
                    <span id="nameError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.lastName" bundle="${label}"/>
                    <input type="text" name="lastName" id="lastName" size="36" class="input" required/><br/>
                    <span id="lastNameError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.email" bundle="${label}"/>
                    <input type="text" name="email" id="email" size="36" class="input" required/><br/>
                    <span id="emailError" style="color: red; font-size: 12px;"></span></br>
                    <fmt:message key="register.phone" bundle="${label}"/>
                    <input type="text" name="phone" id="phone" size="36" class="input" required/><br/>
                    <span>[+77xxxxxxxxx]</span></br>
                    <span id="phoneError" style="color: red; font-size: 12px;"></span></br>
                </div>
            </div>

            <div class="buttons">
                <span class="message">${registerErrorMsg}</span>
                <span class="message">${registerSuccessMsg}</span></br></br>
                <c:remove var="registerErrorMsg" scope="session" />
                <c:remove var="registerSuccessMsg" scope="session" />
                <input type="submit" value="<fmt:message key="register.register" bundle="${label}"/>" class="submit"/>
                <button class="submit" onclick="goBack();"><fmt:message key="register.cancel" bundle="${label}"/></button>
                </br></br>
            </div>
        </form>
        <form name="goBackForm" id="goBackForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_main_page" />
        </form>
    </div>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>
    <script type="text/javascript">
        function goBack() {
            document.getElementById("goBackForm").submit();
        }
        var pass = pass || {};
        pass.error = {
            startText: "<fmt:message key="verify.passText" bundle="${msg}"/>",
            lowerCase: "<fmt:message key="verify.passLowerCase" bundle="${msg}"/>",
            upperCase: "<fmt:message key="verify.passUpperCase" bundle="${msg}"/>",
            number: "<fmt:message key="verify.passNumber" bundle="${msg}"/>",
            size: "<fmt:message key="verify.passSize" bundle="${msg}"/>"
        };
        var iin = iin || {};
        iin.error = { format: "<fmt:message key="verify.iinFormat" bundle="${msg}"/>"};
        var phone = phone || {};
        phone.error = { format: "<fmt:message key="verify.phoneFormat" bundle="${msg}"/>"};
        var email = email || {};
        email.error = { format: "<fmt:message key="verify.email" bundle="${msg}"/>"};
        var login = login || {};
        login.error = { format: "<fmt:message key="verify.login" bundle="${msg}"/>"};

        function verifyName() {
            var pattern = /^[A-Za-z\u0400-\u04FF]+$/;
            var myName = document.getElementById("firstName");
            if(!pattern.test(myName.value)){
                document.getElementById("nameError").textContent = "<fmt:message key="verify.name" bundle="${msg}"/>";
            }
            return pattern.test(myName.value);
        }
        function verifyLastName() {
            var pattern = /^[A-Za-z\u0400-\u04FF]+$/;
            var myLastName = document.getElementById("lastName");
            if(!pattern.test(myLastName.value)){
                document.getElementById("lastNameError").textContent = "<fmt:message key="verify.lastName" bundle="${msg}"/>";
            }
            return pattern.test(myLastName.value);
        }
    </script>
    <script src="${root}/js/validateInput.js" type="text/javascript" charset="UTF-8"></script>
</body>
</html>