<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<c:set value="${sessionScope.get(\"user\")}" var="user" scope="page" />
<fmt:setBundle basename="label" var="label"/>
<fmt:setBundle basename="admin" var="admin"/>
<fmt:setBundle basename="message" var="msg"/>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="myRequest.title" bundle="${label}"/></title>
</head>
<style>
    <c:import url="/css/app_style.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/client_header.jsp" charEncoding = "utf-8"/>
    </header>
    <h2 style="color: #666; text-align:center;"><fmt:message key="main.welcome" bundle="${label}"/> ${user.userFirstName} ${user.userLastName}!!!</h2>
    <c:if test="${not empty noRequest}"><h3 style="color: red; text-align:center;"> ${noRequest} </h3></c:if>
    <c:if test="${not empty deleteSuccessMsg}"><h3 style="color: red; text-align:center;"> ${deleteSuccessMsg} </h3>
    <c:remove var="deleteSuccessMsg" scope="session" /></c:if>
    <c:if test="${not empty deleteErrorMsg}"><h3 style="color: red; text-align:center;"> ${deleteErrorMsg} </h3>
    <c:remove var="deleteErrorMsg" scope="session" /></c:if>

    <div class="profReqContent">
        <c:if test="${empty noRequest}">
            <table class="profTable">
                <tr><td colspan="2">
                <c:if test="${myRequest.requestStatus == 'active'}">
                    <h4 style="color: red; text-align:center;"><fmt:message key="profile.messageActive" bundle="${label}"/> </h4>
                </c:if></td></tr>
                <tr><td><fmt:message key="activeRequest.name" bundle="${admin}" /></td>
                <td>${myRequest.requestOwner.userFirstName} ${myRequest.requestOwner.userLastName}</td></tr>

                <c:if test="${myRequest.requestStatus == 'active'}">
                <form name="updateRequestForm" id="updateForm" method="POST" action="${root}/controller" onsubmit="return verifyCredit();">
                    <tr><td><fmt:message key="activeRequest.creditSumm" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <input type="text" name="creditAmount" id="creditAmount" size="25" required /> </td></tr>
                    <tr><td><fmt:message key="activeRequest.creditPeriod" bundle="${admin}" /></td>
                    <td><input type="text" name="creditPeriod" id="creditPeriod" size="25" required /> </td></tr>
                    <tr><td><fmt:message key="activeRequest.clientSalary" bundle="${admin}" /></td>
                    <td><input type="text" name="clientSalary" id="clientSalary" size="25" required />  </td></tr>
                    <tr><td><fmt:message key="activeRequest.hasSalaryCard" bundle="${admin}" /></td>
                        <td><select id="mfc_salary" class="select" style="width: 150px;" onchange="setMfcSalaryFlag(this);">
                            <option value="true"> <fmt:message key="main.mfcSalaryYes" bundle="${label}"/> </option>
                            <option value="false"> <fmt:message key="main.mfcSalaryNo" bundle="${label}"/> </option>
                        </select> </td> </tr>
                    <tr><td><fmt:message key="search.requestStatus" bundle="${admin}" /></td>
                    <td>${myRequest.requestStatus} </td></tr>
                    <tr><td></td>
                    <td>
                    <input type="hidden" name="command" value="update_credit_request"/>
                     <input type="hidden" name="requestId" value="${myRequest.requestId}"/>
                     <input type="hidden" name="clientMFCSalary" id="clientMFCSalary"/>
                     <input id="updateBtn" type="submit" value="<fmt:message key="profile.update" bundle="${label}"/>" style="width: 100px; cursor: pointer;"/>
                     </td></tr>
                 </form>
                 <form name="deleteRequestForm" method="GET" action="${root}/controller" onsubmit="return confirmDelete();">
                     <tr><td></td>
                     <td>
                     <input id="deleteBtn" type="submit" value="<fmt:message key="profile.delete" bundle="${label}"/>" style="width: 100px; cursor: pointer;"/>
                     <input type="hidden" name="command" value="delete_credit_request"/>
                     <input type="hidden" name="requestId" value="${myRequest.requestId}"/>
                     <input type="hidden" name="userId" value="${user.userId}"/>
                     </td></tr>
                 </form>
               <tr><td colspan="2">
                    <span class="message">${updateErrorMsg}</span>
                    <span class="message">${updateSuccessMsg}</span>
                    <c:remove var="updateSuccessMsg" scope="session" />
                    <c:remove var="updateErrorMsg" scope="session" />
               </td></tr>
               </c:if>
                <c:if test="${myRequest.requestStatus != 'active'}">
                    <tr><td><fmt:message key="activeRequest.creditSumm" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${myRequest.creditSumm}" type="currency"/> </td></tr>
                    <tr><td><fmt:message key="activeRequest.creditPeriod" bundle="${admin}" /></td>
                    <td>${myRequest.creditMonth} </td></tr>
                    <tr><td><fmt:message key="activeRequest.clientSalary" bundle="${admin}" /></td>
                    <td><fmt:formatNumber value="${myRequest.clientSalary}" type="currency"/> </td></tr>
                    <tr><td><fmt:message key="activeRequest.hasSalaryCard" bundle="${admin}" /></td>
                    <c:if test="${myRequest.hasSalaryCard == 'true'}">
                        <td><fmt:message key="main.mfcSalaryYes" bundle="${label}"/></td></tr>
                    </c:if>
                    <c:if test="${myRequest.hasSalaryCard == 'false'}">
                        <td><fmt:message key="main.mfcSalaryNo" bundle="${label}"/></td></tr>
                    </c:if>
                    <tr><td><fmt:message key="search.requestStatus" bundle="${admin}" /></td>
                    <td>${myRequest.requestStatus} </td></tr>
                </c:if>
                <c:if test="${myRequest.requestStatus == 'fail'}">
                    <tr><td colspan="2" style="color: red;"> <fmt:message key="profile.messageFail" bundle="${label}"/> </td></tr>
                </c:if>
                <c:if test="${myRequest.requestStatus == 'success'}">
                <form name="sendDecisionForm" method="POST" action="${root}/controller" onsubmit="return checkDecision()">
                    <tr style="color:blue;"><td><fmt:message key="activeRequest.creditRate" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${myRequest.rate}" maxFractionDigits="2" minIntegerDigits="2" type="percent"/> </td></tr>
                    <tr style="color:blue;"><td><fmt:message key="activeRequest.creditCommission" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${myRequest.commission}" maxFractionDigits="2" maxIntegerDigits="3" type="percent"/> </td></tr>
                    <tr style="color:blue;"><td><fmt:message key="activeRequest.monthlyPayment" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${myRequest.monthlyPayment}" type="currency"/> </td></tr>
                    <tr><td><fmt:setLocale value="${sessionScope.get(\"locale\")}"/>
                            <fmt:message key="profile.clientDecision" bundle="${label}" /></td>
                    <c:if test="${not empty decisionFeedback}">
                        <td> ${myRequest.clientDecision} </td></tr>
                    </c:if>
                    <c:if test="${empty decisionFeedback}">
                        <td><select id="req_decision" style="width: 150px;" onchange="setReqDecision(this);">
                             <option value="accept" selected> <fmt:message key="profile.decAccept" bundle="${label}"/> </option>
                             <option value="deny"> <fmt:message key="profile.decDeny" bundle="${label}"/> </option>
                         </select></td></tr>
                    <tr><td><input type="hidden" name="command" value="send_decision"/>
                            <input type="hidden" name="clientDecision" id="clientDecision"/>
                            <input type="hidden" name="requestId" value="${myRequest.requestId}"/></td>
                            <input type="hidden" name="profileParam" value="${profileParam}"/></td>
                        <td><input type="submit" value="<fmt:message key="profile.sendDecision" bundle="${label}"/>" class="submit"/> </td></tr>
                    <tr><td colspan="2" style="color: red;"> <fmt:message key="profile.messageSuccess" bundle="${label}"/> </td></tr>
                    </c:if>
                    <c:if test="${not empty decisionFeedback}">
                       <tr><td colspan="2" style="color: red;"> ${decisionFeedback} </td></tr>
                       <c:remove var="decisionFeedback" scope="session"/>
                    </c:if>
                </form>
                </c:if>
            </table>
        </c:if>
        <c:remove var="noRequest" scope="session" />
    </div>

    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>
    <script type="text/javascript">
        window.onload = function() {
            document.getElementById("creditAmount").value = "${myRequest.creditSumm}";
            document.getElementById("creditPeriod").value = "${myRequest.creditMonth}";
            document.getElementById("clientSalary").value = "${myRequest.clientSalary}";
            var obj = document.getElementById("mfc_salary");
            document.getElementById("clientMFCSalary").value = "${myRequest.hasSalaryCard}";
            if("${myRequest.hasSalaryCard}" == "true") {
                obj.options[0].selected = true;
            } else {
                obj.options[1].selected = true;
            }
        }
        var sum = sum || {};
        sum.content = { boundary: "<fmt:message key="verify.sum" bundle="${msg}"/>",
                        error: "<fmt:message key="verify.sumError" bundle="${msg}"/>" };
        var period = period || {};
        period.content = { boundary: "<fmt:message key="verify.month" bundle="${msg}"/>",
                        error: "<fmt:message key="verify.monthError" bundle="${msg}"/>" };
        var salary = salary || {};
        salary.content = {  limit: "<fmt:message key="verify.salary" bundle="${msg}"/>",
                            error: "<fmt:message key="verify.salaryError" bundle="${msg}"/>"};
        var confirm = confirm || {};
        confirm.content = { text: "<fmt:message key="verify.submit" bundle="${msg}"/>"};
        var clean = clean || {};
        clean.content = { text: "<fmt:message key="verify.delete" bundle="${msg}"/>"};

        function setReqDecision(decisionObj) {
            var value = decisionObj.value;
            document.getElementById("clientDecision").value = value;
        }

        function checkDecision() {
            alert(document.getElementById("clientDecision").value);
            return window.confirm("<fmt:message key="profile.validateMsg" bundle="${label}" />");
        }
    </script>
    <script src="${root}/js/validateCreditInput.js" type="text/javascript"></script>
    <script src="${root}/js/requestAction.js" type="text/javascript"></script>
</body>
</html>