<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<fmt:setBundle basename="admin" var="admin"/>
<html>
<head><title>Header</title>
</head>
<body>
    <div class="header">
        <form name="openMainForm" id="logo_click" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_main_page"/>
            <div id="logo" class="logo">
                <input type="submit" value="" class="logoButton" style="background-image: url('${root}/images/logo.png')"/>
            </div>
        </form>
        <div id="companyName" class="title"><fmt:message key="header.title" bundle="${label}"/></div>
        <div id="menuBox" class="operation_Box">
            <button onclick="myFunction()" class="dropbtn">
                <fmt:message key="admin.menu" bundle="${admin}"/>
            </button>
            <div id="myDropdown" class="dropdown-content">
                <input type="button" value="<fmt:message key="adminMenu.search" bundle="${admin}"/>" class="menuopt" onclick="searchRequest()"/>
                <input type="button" value="<fmt:message key="adminMenu.newRequests" bundle="${admin}"/>" class="menuopt" onclick="getRequest()"/>
                <input type="button" value="<fmt:message key="adminMenu.back" bundle="${admin}"/>" class="menuopt" onclick="history.back(); return false;"/>
            </div>
        </div>
        <form name="searchForm" id="searchForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_request_page"/>
        </form>
        <form name="requestForm" id="requestForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_request_process_page"/>
        </form>
        <form name="logoutForm" method="POST" action="${root}/controller">
            <input type="hidden" name="command" value="logout"/>
            <div id="logout" class="signIn">
                <input type="submit" value="<fmt:message key="main.logout" bundle="${label}"/>" class="loginButton"/>
            </div>
        </form>
    </div>
    <script src="${root}/js/profileMenu.js" type="text/javascript"></script>
</body>
</html>