<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<html>
<head><title>Header</title>
</head>
<body>
    <div class="header">
        <form name="openMainForm" id="logo_click" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_main_page"/>
            <div id="logo" class="logo">
                <input type="submit" value="" class="logoButton" style="background-image: url('${root}/images/logo.png')"/>
            </div>
        </form>
        <div id="companyName" class="title"><fmt:message key="header.title" bundle="${label}"/></div>
        <form name="logoutForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="logout"/>
            <div id="logout" class="signIn">
                <input type="submit" value="<fmt:message key="main.logout" bundle="${label}"/>" class="loginButton"/>
            </div>
        </form>
    </div>
</body>
</html>