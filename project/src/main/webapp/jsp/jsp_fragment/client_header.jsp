<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<c:set value="${sessionScope.get(\"user\")}" var="user" scope="page" />
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<html>
<head><title>Header</title>
</head>
<body>
    <div class="header">
        <form name="openMainForm" id="logo_click" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_main_page"/>
            <div id="logo" class="logo">
                <input type="submit" value="" class="logoButton" style="background-image: url('${root}/images/logo.png')"/>
            </div>
        </form>
        <div id="companyName" class="title"><fmt:message key="header.title" bundle="${label}"/></div>
        <c:if test="${not empty user}">
        <div id="menuBox" class="lang_Box">
            <button onclick="myFunction()" class="dropbtn">
                <fmt:message key="main.profile" bundle="${label}"/>
            </button>
            <div id="myDropdown" class="dropdown-content">
                <input type="button" value="<fmt:message key="profMenu.profile" bundle="${label}"/>" class="langopt" onclick="submitProfile()"/>
                <input type="button" value="<fmt:message key="profMenu.request" bundle="${label}"/>" class="langopt" onclick="submitMyRequest()"/>
                <input type="button" value="<fmt:message key="profMenu.back" bundle="${label}"/>" class="langopt" onclick="history.back(); return false;"/>
            </div>
        </div>
        <form name="goProfileForm" id="goProfileForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_profile"/>
            <input type="hidden" name="userId" value="${user.userId}"/>
        </form>
        <form name="goMyRequestForm" id="goMyRequestForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_my_request"/>
            <input type="hidden" name="userId" value="${user.userId}"/>
        </form>
        <form name="logoutForm" method="POST" action="${root}/controller">
            <input type="hidden" name="command" value="logout"/>
            <div id="logout" class="signIn">
                <input type="submit" value="<fmt:message key="main.logout" bundle="${label}"/>" class="loginButton"/>
            </div>
        </form>
        </c:if>
        <c:if test="${empty user}">
        <div id="langBox" class="lang_Box">
            <button onclick="myFunction()" class="dropbtn">
                <fmt:message key="header.langButtonName" bundle="${label}"/>
            </button>
            <div id="myDropdown" class="dropdown-content">
                <input type="button" value="EN" class="langopt" onclick="langSelect(this)"/>
                <input type="button" value="RU" class="langopt" onclick="langSelect(this)"/>
            </div>
        </div>
        <form name="setEnLang" id="enLang" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="change_language"/>
            <input type="hidden" name="lang" id="lang_id"/>
        </form>
        <form name="openLoginForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_login_page"/>
            <div id="login" class="signIn">
                <input type="submit" value="<fmt:message key="login.login" bundle="${label}"/>" class="loginButton"/>
            </div>
        </form>
        </c:if>
    </div>
    <script src="${root}/js/langMenu.js" type="text/javascript"></script>
    <script src="${root}/js/profileMenu.js" type="text/javascript"></script>
</body>
</html>