<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<html>
<head><title>Header</title>
</head>
<body>
    <div class="header">
        <form name="openMainForm" id="logo_click" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_main_page"/>
            <div id="logo" class="logo">
                <input type="submit" value="" class="logoButton" style="background-image: url('${root}/images/logo.png')"/>
            </div>
        </form>
        <div id="companyName" class="title"><fmt:message key="header.title" bundle="${label}"/></div>
        <div id="langBox" class="lang_Box">
            <button onclick="myFunction()" class="dropbtn">
                <fmt:message key="header.langButtonName" bundle="${label}"/>
            </button>
            <div id="myDropdown" class="dropdown-content">
                <input type="button" value="EN" class="langopt" onclick="langSelect(this)"/>
                <input type="button" value="RU" class="langopt" onclick="langSelect(this)"/>
            </div>
        </div>
        <form name="setEnLang" id="enLang" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="change_language"/>
            <input type="hidden" name="lang" id="lang_id"/>
        </form>
        <form name="openLoginForm" method="GET" action="${root}/controller">
            <input type="hidden" name="command" value="open_login_page"/>
            <div id="login" class="signIn">
                <input type="submit" value="<fmt:message key="login.login" bundle="${label}"/>" class="loginButton"/>
            </div>
        </form>
    </div>
    <script src="${root}/js/langMenu.js" type="text/javascript">
    </script>
</body>
</html>