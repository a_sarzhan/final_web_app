<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<c:set value="${sessionScope.get(\"user\")}" var="user" scope="page" />
<c:set value="${pageContext.request.contextPath}" var="root" scope="session"/>
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<fmt:setBundle basename="message" var="msg"/>
<html>
<head>
    <title><fmt:message key="main.title" bundle="${label}"/></title>
</head>
<style>
    <c:import url="/css/app_style.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/client_header.jsp" charEncoding = "utf-8"/>
    </header>
    <div class="mainContent">
        <c:if test="${empty user}">
            <form name="openRegisterForm" method="GET" action="${root}/controller">
                <input type="hidden" name="command" value="open_register_page" />
                <div class="mainRow"><fmt:message key="header.title" bundle="${label}"/></div>
                <div class="mainRow">
                <input type="submit" value="<fmt:message key="main.newClient" bundle="${label}"/>" class="newUserButton"/>
                </div>
            </form>
        </c:if>
        <c:if test="${not empty user}">
            <form name="formRequest" method="POST" action="${root}/controller" onsubmit="return verifyCredit();">
                <input type="hidden" name="command" value="credit_request" />
                <input type="hidden" name="userId" value="${user.userId}" />
                <input type="hidden" name="clientMFCSalary" id="clientMFCSalary" required/>

                <div class="welcome">
                <fmt:message key="main.welcome" bundle="${label}"/> ${user.userFirstName} ${user.userLastName}!!!
                </div>
                <div class="newRequest" onchange="requestEdit();">
                    <fmt:message key="main.creditAmount" bundle="${label}" />
                    <input type="text" name="creditAmount" id="creditAmount" size="36" class="input" required/><br/>
                    <fmt:message key="main.creditPeriod" bundle="${label}" />
                    <input type="text" name="creditPeriod" id="creditPeriod" size="36" class="input" required/><br/>
                    <fmt:message key="main.creditSalary" bundle="${label}" />
                    <input type="text" name="clientSalary" id="clientSalary" size="36" class="input" required/><br/>
                    <fmt:message key="main.creditMFCSalary" bundle="${label}" />
                    <select id="mfc_salary" class="select">
                        <option value="true" selected="selected"> <fmt:message key="main.mfcSalaryYes" bundle="${label}"/> </option>
                        <option value="false"> <fmt:message key="main.mfcSalaryNo" bundle="${label}"/> </option>
                    </select>
                </div>
                </br></br>
                <span id="creditSuccess" class="message">${requestSuccessMsg}</span>
                <span id="creditFail" class="message">${requestErrorMsg}</span>
                <c:remove var="requestSuccessMsg" scope="session" />
                <c:remove var="requestErrorMsg" scope="session" />
                </br>
                <div class="buttons">
                    <input type="submit" value="<fmt:message key="main.sendRequest" bundle="${label}"/>" class="submit"/>
                </div>
            </form>
        </c:if>
    </div>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>

    <script type="text/javascript">
        function requestEdit() {
            var obj = document.getElementById("mfc_salary");
            for (var i = 0; i < obj.options.length; i++) {
                if(obj.options[i].selected === true) {
                    document.getElementById("clientMFCSalary").value = obj.options[i].value;
                    //alert(document.getElementById("clientMFCSalary").value);
                    break;
                }
            }
        }
        var sum = sum || {};
        sum.content = { boundary: "<fmt:message key="verify.sum" bundle="${msg}"/>",
                        error: "<fmt:message key="verify.sumError" bundle="${msg}"/>" };
        var period = period || {};
        period.content = { boundary: "<fmt:message key="verify.month" bundle="${msg}"/>",
                        error: "<fmt:message key="verify.monthError" bundle="${msg}"/>" };
        var salary = salary || {};
        salary.content = {  limit: "<fmt:message key="verify.salary" bundle="${msg}"/>",
                            error: "<fmt:message key="verify.salaryError" bundle="${msg}"/>"};
        var confirm = confirm || {};
        confirm.content = { text: "<fmt:message key="verify.submit" bundle="${msg}"/>"};
    </script>
    <script src="${root}/js/validateCreditInput.js" type="text/javascript"></script>
</body>
</html>