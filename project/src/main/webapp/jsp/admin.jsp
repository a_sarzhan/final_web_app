<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"manager\")}" var="manager" scope="page" />
<fmt:setBundle basename="admin" var="admin"/>
<html>
<head>
    <title><fmt:message key="admin.title" bundle="${admin}"/></title>
</head>
<style>
    <c:import url="/css/admin.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/admin_header.jsp" charEncoding = "utf-8"/>
    </header>
    <div class="welcome">
        <fmt:message key="admin.welcome" bundle="${admin}"/> ${manager.managerFullName}!!!
    </div>
    <div class="mainTab">
    </div>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>

</body>
</html>