<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"locale\").language}" var="lang" scope="page" />
<fmt:setLocale value="${sessionScope.get(\"locale\")}" />
<fmt:setBundle basename="label" var="label"/>
<html>
<head>
    <title><fmt:message key="login.title" bundle="${label}"/></title>
</head>
<style>
    <c:import url="/css/app_style.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/header.jsp" charEncoding = "utf-8"/>
    </header>
    </br>
    <h1><fmt:message key="login.title" bundle="${label}"/></h1>
    <div class="tabContainer">
        <div class="buttonContainer">
            <button onclick="showPanel(0, '#B4C68B')"><fmt:message key="login.client" bundle="${label}"/></button>
            <button onclick="showPanel(1, '#93B9AC')"><fmt:message key="login.manager" bundle="${label}"/></button>
        </div>
        <div class="tabPanel">
             <form name="loginForm" method="POST" action="${root}/controller">
             <input type="hidden" name="command" value="login"/>
             <input type="hidden" name="userRole" value="client"/>
                <div class="login_div">
                    <fmt:message key="login.userName" bundle="${label}"/>
                    <input type="text" name="userName" size="36" required class="input"/></br>
                    <fmt:message key="login.password" bundle="${label}"/>
                    <input type="password" id="psw1" name="password" size="36" required class="input"/></br>
                </div>
                </br>
                <span id="userError" class="error_message">${errorUserLoginMsg}</span>
                <div class="submit_div">
                    <input type="submit" value="<fmt:message key="login.login" bundle="${label}"/>" class="submit"/></br>
                </div>
             </form>
        </div>
        <div class="tabPanel">
            <form name="loginForm" method="POST" action="${root}/controller">
            <input type="hidden" name="command" value="login"/>
            <input type="hidden" name="userRole" value="manager"/>
                <div class="login_div">
                    <fmt:message key="login.managerId" bundle="${label}"/>
                    <input type="text" name="managerId" size="36" required class="input"/></br>
                    <fmt:message key="login.password" bundle="${label}"/>
                    <input type="password" id="psw2" name="managerPassword" size="36" required class="input"/>
                </div>
                </br>
                <span id="managerError" class="error_message">${errorManagerLoginMsg}</span>
                <div class="submit_div">
                    <input type="submit" value="<fmt:message key="login.login" bundle="${label}"/>" class="submit"/></br>
                </div>
            </form>
        </div>
    </div>
    </br>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>
    <script src="js/loginTab.js" type="text/javascript"></script>
    <script type="text/javascript">
        var mngError = '${errorManagerLoginMsg}';
        if (mngError) {
            showPanel(1, '#93B9AC');
        }
    </script>
</body>
</html>