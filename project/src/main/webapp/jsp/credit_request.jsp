<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"creditRequest\")}" var="creditRequest" scope="page" />
<c:set value="${pageScope.get(\"pageNo\")}" var="pageNo" scope="page" />
<fmt:setBundle basename="admin" var="admin"/>
<html>
<head>
    <title><fmt:message key="requestSearch.title" bundle="${admin}"/></title>
</head>
<style>
    <c:import url="/css/admin.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/admin_header.jsp" charEncoding = "utf-8"/>
    </header>
    <div class="welcome">
        <fmt:message key="admin.welcome" bundle="${admin}"/> ${manager.managerFullName}!!!
    </div>
    <div class="requestMain">
        <div class="requestSearch">
            <form name="requestSearchForm" method="GET" action="${root}/controller">
                <input type="hidden" name="command" value="request_search"/>
                <input type="hidden" name="pageNo" value="${pageNo}"/>
                <fmt:message key="search.IIN" bundle="${admin}" /><br/>
                <input type="text" name="IIN" size="24"/><br/><br/>
                <fmt:message key="search.requestDateFrom" bundle="${admin}" /><br/>
                <input type="date" id="startDate" min="2012-01-01" max="2022-12-31"
                    name="requestDateFrom" size="24"/><br/><br/>
                <fmt:message key="search.requestDateTo" bundle="${admin}" /><br/>
                <input type="date" id="endDate" min="2012-01-01" max="2022-12-31"
                    name="requestDateTo" size="24"/><br/><br/>

                <fmt:message key="search.requestStatus" bundle="${admin}" /><br/>
                <select id="req_status" style="width: 190px;" onchange="setReqStatus(this);">
                    <option value=""> </option>
                    <option value="active"> <fmt:message key="search.select.reqActive" bundle="${admin}"/> </option>
                    <option value="success"> <fmt:message key="search.select.reqSuccess" bundle="${admin}"/> </option>
                    <option value="fail"> <fmt:message key="search.select.reqFail" bundle="${admin}"/> </option>
                    <option value="inactive"> <fmt:message key="search.select.reqInactive" bundle="${admin}"/> </option>
                </select></br></br>
                <input type="hidden" name="requestStatus" id="requestStatus"/>

                <fmt:message key="search.clientDecision" bundle="${admin}" /><br/>
                <select id="req_decision" style="width: 190px;" onchange="setReqDecision(this);">
                    <option value=""> </option>
                    <option value="accept"> <fmt:message key="search.select.decAccept" bundle="${admin}"/> </option>
                    <option value="deny"> <fmt:message key="search.select.decDeny" bundle="${admin}"/> </option>
                </select></br></br>
                <input type="hidden" name="clientDecision" id="clientDecision"/>
                <input type="submit" value="<fmt:message key="request.newSearch" bundle="${admin}"/>" class="submit"/>
            </form>
        </div>
        <c:if test="${not empty requestsNotFound}">
            <div class="not_found"> ${requestsNotFound} </div>
        </c:if>
        <c:if test="${empty requestsNotFound}">
        <div class="foundData">
            <table id="all_request">
            <c:forEach items="${creditRequest}" var="credit" varStatus="loop">
                <c:if test="${loop.index == 0}">
                <tr>
                    <td><fmt:message key="request.Index" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.creditSumm" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.creditPeriod" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.creditRate" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.creditCommission" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.monthlyPayment" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.clientSalary" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.hasSalaryCard" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.date" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.status" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.clientDecision" bundle="${admin}" /> </td>
                    <td><fmt:message key="request.requestOwner" bundle="${admin}" /> </td>
                </tr>
                </c:if>
                <tr>
                    <td>${loop.index + 1 + (pageNo - 1) * 10} </td>
                    <td><fmt:setLocale value="ru-KZ"/>
                        <fmt:formatNumber value="${credit.creditSumm}" type="currency"/> </td>
                    <td><fmt:formatNumber value="${credit.creditMonth}" /> </td>
                    <c:if test="${credit.requestStatus == 'active'}">
                        <td></td>
                        <td></td>
                        <td></td>
                    </c:if>
                    <c:if test="${credit.requestStatus != 'active'}">
                        <td><fmt:formatNumber value="${credit.rate}" type="percent"/> </td>
                        <td><fmt:formatNumber value="${credit.commission}" type="percent"/> </td>
                        <td><fmt:formatNumber value="${credit.monthlyPayment}" type="currency"/> </td>
                    </c:if>
                    <td><fmt:formatNumber value="${credit.clientSalary}" type="currency"/> </td>
                    <td>${credit.hasSalaryCard} </td>
                    <td><fmt:setLocale value="en-US"/>
                        <fmt:formatDate value="${credit.requestDate}" pattern="dd.MM.yyyy"/> </td>
                    <td>${credit.requestStatus} </td>
                    <c:if test="${credit.requestStatus == 'active'}">
                        <td></td>
                    </c:if>
                    <c:if test="${credit.requestStatus != 'active'}">
                        <td>${credit.clientDecision} </td>
                    </c:if>
                    <td>${credit.requestOwner.userIIN} </td>
                </tr>
            </c:forEach>
            </table>
           <div class="paging">
                <c:forEach var="i" begin="1" end="${totalPages}">
                    <c:if test="${i==pageNo}">
                        <a class="active" href="${root}/controller/?command=request_search&pageNo=${i}&IIN=${iin}&requestDateFrom=${dateFrom}&requestDateTo=${dateTo}&requestStatus=${status}&clientDecision=${decision}">${i}</a>
                    </c:if>
                    <c:if test="${i!=pageNo}">
                        <a href="${root}/controller/?command=request_search&pageNo=${i}&IIN=${iin}&requestDateFrom=${dateFrom}&requestDateTo=${dateTo}&requestStatus=${status}&clientDecision=${decision}">${i}</a>
                    </c:if>
                </c:forEach>
                </br></br>
            </div>
        </div>
        </c:if>
    </div>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>
    <script type="text/javascript">
        /*var today = new Date();
        var currentDate = today.toISOString().slice(0,10);
        document.getElementById("startDate").value = currentDate;
        document.getElementById("endDate").value = currentDate;*/

        function setReqStatus(statusObj) {
        var value = statusObj.value;
        document.getElementById("requestStatus").value = value;
            //alert(document.getElementById("requestStatus").value);
        }

        function setReqDecision(decisionObj) {
        var value = decisionObj.value;
        document.getElementById("clientDecision").value = value;
            //alert(document.getElementById("clientDecision").value);
        }
    </script>
</body>
</html>