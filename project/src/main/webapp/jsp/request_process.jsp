<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="entity.CreditRequest"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set value="${sessionScope.get(\"activeRequest\")}" var="activeRequest" scope="page" />
<c:set value="${pageScope.get(\"pageNo\")}" var="pageNo" scope="page" />
<c:set value="${pageScope.get(\"inTerms\")}" var="inTerms" scope="page" />
<fmt:setBundle basename="admin" var="admin"/>
<fmt:setBundle basename="label" var="label"/>
<fmt:setBundle basename="message" var="msg"/>
<html>
<head>
    <title><fmt:message key="request.title" bundle="${admin}"/></title>
</head>
<style>
    <c:import url="/css/admin.css" charEncoding = "utf-8"/>
</style>
<body>
    <header>
        <c:import url="jsp_fragment/admin_header.jsp" charEncoding = "utf-8"/>
    </header>

    <div class="welcome">
        <fmt:message key="admin.welcome" bundle="${admin}"/> ${manager.managerFullName}!!!
    </div>
    <div class="activeContainer">
        <c:if test="${not empty activeRequestNotFound}">
            <div class="not_found"> ${activeRequestNotFound} </div>
        </c:if>
        <form name="GetConditionForm" method="GET" action="${root}/controller" onsubmit="return validateForm()">
        <div class="elementBox">
            <c:forEach items="${activeRequest}" var="activeReq" varStatus="loop">
                <table id="reqTable">
                <tr><td><fmt:message key="activeRequest.requestID" bundle="${admin}" /></td>
                <td>${activeReq.requestId} </td></tr>
                <tr><td><fmt:message key="activeRequest.creditSumm" bundle="${admin}" /></td>
                <td><fmt:setLocale value="ru-KZ"/>
                <fmt:formatNumber value="${activeReq.creditSumm}" type="currency"/> </td></tr>
                <tr><td><fmt:message key="activeRequest.creditPeriod" bundle="${admin}" /></td>
                <td>${activeReq.creditMonth} </td></tr>
                <tr><td><fmt:message key="activeRequest.clientSalary" bundle="${admin}" /></td>
                <td><fmt:formatNumber value="${activeReq.clientSalary}" type="currency"/> </td></tr>
                <tr><td><fmt:message key="activeRequest.hasSalaryCard" bundle="${admin}" /></td>
                <c:if test="${activeReq.hasSalaryCard == 'true'}">
                    <td><fmt:message key="main.mfcSalaryYes" bundle="${label}"/></td></tr>
                </c:if>
                <c:if test="${activeReq.hasSalaryCard == 'false'}">
                    <td><fmt:message key="main.mfcSalaryNo" bundle="${label}"/></td></tr>
                </c:if>
                <tr><td><fmt:message key="activeRequest.date" bundle="${admin}" /></td>
                <td><fmt:setLocale value="en-US"/>
                <fmt:formatDate value="${activeReq.requestDate}" pattern="dd.MM.yyyy"/> </td></tr>
                <tr><td><fmt:message key="activeRequest.status" bundle="${admin}" /></td>
                <td>${activeReq.requestStatus} </td></tr>
                <tr><td><fmt:message key="activeRequest.IIN" bundle="${admin}" /></td>
                <td>${activeReq.requestOwner.userIIN} </td></tr>
                <tr><td><fmt:message key="activeRequest.name" bundle="${admin}" /></td>
                <td>${activeReq.requestOwner.userFirstName} ${activeReq.requestOwner.userLastName} </td></tr>
                <c:if test="${not empty inTerms}">
                    <tr><td><fmt:message key="activeRequest.creditRate" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${activeReq.rate}" maxFractionDigits="2" minIntegerDigits="2" type="percent"/> </td></tr>
                    <tr><td><fmt:message key="activeRequest.creditCommission" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${activeReq.commission}" maxFractionDigits="2" maxIntegerDigits="3" type="percent"/> </td></tr>
                    <tr><td><fmt:message key="activeRequest.monthlyPayment" bundle="${admin}" /></td>
                    <td><fmt:setLocale value="ru-KZ"/>
                    <fmt:formatNumber value="${activeReq.monthlyPayment}" type="currency"/> </td></tr>
                    <c:if test="${activeReq.requestStatus == 'fail'}">
                        <tr><td><fmt:message key="fail.reason" bundle="${admin}" /></td>
                        <td>${failReason} </td></tr>
                        <c:remove var="failReason" scope="session"/>
                    </c:if>
                </c:if>
                <c:if test="${empty inTerms}">
                <tr><td><fmt:setLocale value="en-US"/>
                <input type="checkbox" name="chk" onclick="getClickedData();" class="chkBox" value="${activeReq.requestId}"/></td>
                </td></tr>
                </c:if>
                </table>
            </c:forEach>
        </div>
        <div class="buttonBox">
            <input type="hidden" name="command" value="process_request"/>
            <input type="hidden" name="creditList" id="creditList"/>
            <input type="hidden" name="pageNo" value="${pageNo}"/>
            <c:if test="${empty inTerms}">
                <input type="submit" value="<fmt:message key="activeRequest.send" bundle="${admin}"/>" class="submit"/>
            </c:if>
        </div>
       <div class="paging">
            <c:forEach var="i" begin="1" end="${totalPages}">
                <c:if test="${i==pageNo}">
                    <a class="active" href="${root}/controller/jsp/request_process.jsp?command=open_request_process_page&pageNo=${i}&creditList=${creditList}">${i}</a>
                </c:if>
                <c:if test="${i!=pageNo}">
                    <a href="${root}/controller/jsp/request_process.jsp?command=open_request_process_page&pageNo=${i}&creditList=${creditList}">${i}</a>
                </c:if>
            </c:forEach>
            </br></br>
        </div>
        </form>
    </div>
    <footer>
        <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
    </footer>
    <script type="text/javascript">
        function validateForm() {
            let chk = document.getElementsByClassName("chkBox");
            var checked = false;
            for(let i = 0; i < chk.length; i++) {
                if(chk[i].checked) {
                    checked = true;
                    break;
                }
            }
            if (!checked) {
                alert("<fmt:message key="request.checker" bundle="${msg}" />");
                return false;
            }
        }

        function getClickedData() {
            let chk = document.getElementsByClassName("chkBox");
            let requestList = "${checkedList}";
            for(let i = 0; i < chk.length; i++) {
                if(chk[i].checked) {
                    requestList += chk[i].value + ",";
                }
            }
            var finalList = requestList.substring(0, requestList.length - 1);
            document.getElementById("creditList").value = requestList;
        }

    </script>
</body>
</html>