<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="label" var="label"/>
<html>
<head>
    <title><fmt:message key="error.title" bundle="${label}"/></title>
</head>
<style>
    <c:import url="/css/app_style.css" charEncoding = "utf-8"/>
</style>
<body>
<header>
    <div class="header">
        <div id="companyName" class="title"><fmt:message key="header.title" bundle="${label}"/></div>
    </div>
</header>
<h1><fmt:message key="error.title" bundle="${label}"/></h1>
<div class = register_div>
    <form action="/auth" method="POST" enctype="multipart/form-data">
        <h3 style="color: red;"><c:out value="${systemError}" /></h3>
        <c:remove var="systemError" scope="session" />
        <input type="submit" class="submit" value="<fmt:message key="error.backBtn" bundle="${label}"/>" onclick="history.back(); return false;"/>
    </form>
</div>
<footer>
    <c:import url="jsp_fragment/footer.jsp" charEncoding = "utf-8"/>
</footer>
</body>
</html>