package validation;

import entity.User;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InputValidatorTest {
    private final InputValidator validator = InputValidator.getValidator();

    @Test
    public void testCreditRequestFieldsPass() {
        assertTrue(validator.isValidCreditRequestFields(100_000, 3, 90_000));
        assertTrue(validator.isValidCreditRequestFields(7000_000, 72, 1500_000));
        assertTrue(validator.isValidCreditRequestFields(2500_000, 36, 600_000));
    }

    @Test
    public void testCreditRequestFieldsFail() {
        assertFalse(validator.isValidCreditRequestFields(-100_000, 3, 90_000));
        assertFalse(validator.isValidCreditRequestFields(7100_000, 72, 1500_000));
        assertFalse(validator.isValidCreditRequestFields(1000_000, 2, 250_000));
        assertFalse(validator.isValidCreditRequestFields(3400_000, 84, 600_000));
        assertFalse(validator.isValidCreditRequestFields(250_000, 24, 300_000));
    }

    @Test
    public void testUserInfoPass() {
        User testUser = new User();
        testUser.setUserLogin("Madison77_07");
        testUser.setUserPassword("jgk7890_$AS");
        testUser.setUserIIN("900807600500");
        testUser.setUserFirstName("Madison");
        testUser.setUserLastName("Carlson");
        testUser.setUserEmail("madi77_07@mail.ru");
        testUser.setUserPhone("+77273675747");
        assertTrue(validator.isValidUserInput(testUser));

        testUser.setUserLogin("kiR_89.-09");
        testUser.setUserPassword("tyi6438@&$_ABS");
        testUser.setUserIIN("990706500400");
        testUser.setUserFirstName("Кирилл");
        testUser.setUserLastName("Соколов");
        testUser.setUserEmail("kir99_07@some.com");
        testUser.setUserPhone("+77012223334");
        assertTrue(validator.isValidUserInput(testUser));

    }

    @Test
    public void testUserInputFail() {
        User testUser = new User();
        testUser.setUserLogin("simpSON_89-89");
        testUser.setUserPassword("oi543DA@@@");
        testUser.setUserIIN("890213657657");
        testUser.setUserFirstName("Simpson");
        testUser.setUserLastName("Johnson");
        testUser.setUserEmail("simpo@_89@mail.ru");
        testUser.setUserPhone("+770788899001");
        assertFalse(validator.isValidUserInput(testUser));

        testUser.setUserLogin("rocket_.-01");
        testUser.setUserPassword("tuio6438@&$fS");
        testUser.setUserIIN("990706500400");
        testUser.setUserFirstName("Степан");
        testUser.setUserLastName("Кузнецов_777");
        testUser.setUserEmail("rock@rocket.com");
        testUser.setUserPhone("+77023334447");
        assertFalse(validator.isValidUserInput(testUser));
    }
}