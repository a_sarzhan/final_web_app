package dao.implementation;

import connection.ProxyConnection;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class AccountDaoTest {
    @Mock
    ProxyConnection mockConnection;
    @Mock
    ResultSet mockSet;
    @Mock
    PreparedStatement mockStatement;
    @InjectMocks
    AccountDao testDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAccountIdByIIN_Positive() throws SQLException {
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockStatement);
        doNothing().when(mockStatement).setString(anyInt(), anyString());
        when(mockStatement.executeQuery()).thenReturn(mockSet);
        when(mockSet.next()).thenReturn(Boolean.TRUE);
        when(mockSet.getInt(anyString())).thenReturn(1000);

        int actualResult = testDao.findAccountIdByIIN("880809760700");
        assertTrue(actualResult > 0);
        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockStatement, times(1)).executeQuery();
    }

    @Test
    public void testFindAccountIdByIIN_Negative() throws SQLException {
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockStatement);
        doNothing().when(mockStatement).setString(anyInt(), anyString());
        when(mockStatement.executeQuery()).thenReturn(mockSet);
        when(mockSet.next()).thenReturn(Boolean.FALSE);

        int actualResult = testDao.findAccountIdByIIN("880809760700");
        assertEquals(0, actualResult);
        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockStatement, times(1)).executeQuery();
        verify(mockSet, times(0)).getInt(anyString());
    }
}