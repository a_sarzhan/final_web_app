package dao.implementation;

import connection.ProxyConnection;
import exceptions.UserNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class UserDaoTest {
    @Mock
    ProxyConnection mockConnection;
    @Mock
    PreparedStatement mockStatement;
    @Mock
    ResultSet mockSet;
    @InjectMocks
    UserDao testDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindUserByLoginAndPassword_Positive() throws SQLException, UserNotFoundException {
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockStatement);
        doNothing().when(mockStatement).setString(anyInt(), anyString());
        when(mockStatement.executeQuery()).thenReturn(mockSet);
        when(mockSet.next()).thenReturn(true);

        assertNotNull(testDao.findUserByNamePassword("sunny", "Pass9900"));
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindUserByLoginAndPassword_Negative() throws SQLException, UserNotFoundException {
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockStatement);
        doNothing().when(mockStatement).setString(anyInt(), anyString());
        when(mockStatement.executeQuery()).thenReturn(mockSet);
        when(mockSet.next()).thenReturn(false);

        assertNull(testDao.findUserByNamePassword("sunny", "Pass9900"));
    }
}