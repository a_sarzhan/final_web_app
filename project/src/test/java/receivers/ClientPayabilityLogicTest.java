package receivers;

import dao.implementation.TransactionDao;
import entity.CreditRequest;
import exceptions.TransactionException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ClientPayabilityLogicTest {
    @Mock
    TransactionDao dao;
    @InjectMocks
    ClientPayabilityLogic payabilityLogic;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreditTermsMFC_Pass() throws SQLException, TransactionException {
        when(dao.getTransactionsCountByDate(any(Integer.class), any(Date.class))).thenReturn(6);
        when(dao.getTransactionSumByDate(any(Integer.class), any(Date.class))).thenReturn((double) 2400_000);

        CreditRequest creditRequest = new CreditRequest();
        creditRequest.setCreditSumm(2500_000);
        creditRequest.setCreditMonth(24);
        creditRequest.setClientSalary(400_000);

        assertNotNull("Credit terms not null, success", payabilityLogic.getCreditTermsMfcClient(3, dao, creditRequest));
    }

    @Test
    public void testCreditTermsMFC_Fail() throws SQLException {
        when(dao.getTransactionsCountByDate(any(Integer.class), any(Date.class))).thenReturn(2);
        CreditRequest creditRequest = new CreditRequest();
        creditRequest.setCreditSumm(3500_000);
        creditRequest.setCreditMonth(36);
        creditRequest.setClientSalary(200_000);
        Map<String, Double> termsMap = new HashMap<>();;
        try {
            termsMap = payabilityLogic.getCreditTermsMfcClient(2, dao, creditRequest);
        } catch (TransactionException e) {
            assertEquals("Credit terms empty, fail", true, termsMap.isEmpty());
        }
    }
}