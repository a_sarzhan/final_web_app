CREATE DATABASE finance;

CREATE TABLE finance.managers (
  manager_id SMALLINT NOT NULL AUTO_INCREMENT,
  manager_login VARCHAR(128) UNIQUE,
  manager_password VARCHAR(128) UNIQUE,
  manager_full_name VARCHAR(128) NOT NULL,
  manager_phone VARCHAR(32) NOT NULL DEFAULT '',
  manager_email VARCHAR(48) NOT NULL DEFAULT '',
  manager_branch VARCHAR(128) NOT NULL DEFAULT '',
  PRIMARY KEY(manager_id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE finance.users (
  user_id SMALLINT NOT NULL AUTO_INCREMENT,
  user_login VARCHAR(128) UNIQUE,
  user_password VARCHAR(128) UNIQUE,
  user_IIN VARCHAR(12) UNIQUE NOT NULL,
  user_name VARCHAR(128) NOT NULL,
  user_lastname VARCHAR(32) NOT NULL,
  user_email VARCHAR(48) NOT NULL DEFAULT '',
  user_phone VARCHAR(128) NOT NULL DEFAULT '',
  PRIMARY KEY (user_id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE finance.creditRequests (
	request_id SMALLINT NOT NULL AUTO_INCREMENT,
    credit_summ INT NOT NULL,
    credit_month SMALLINT NOT NULL,
    salary_card BOOL NOT NULL,
    client_salary DOUBLE,
    request_date DATE,
    request_status VARCHAR(50) NOT NULL,
	rate DOUBLE, 
	commission DOUBLE,
	monthly_payment DOUBLE,
	client_decision VARCHAR(10),
    user_id SMALLINT,
    PRIMARY KEY (request_id),
	CONSTRAINT FK_UserRequest FOREIGN KEY (user_id)
	REFERENCES finance.users (user_id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE finance.accounts (
	account_id SMALLINT NOT NULL AUTO_INCREMENT,
    account_number VARCHAR(20) NOT NULL,
    card_number VARCHAR(16),
    card_type VARCHAR(50),
    start_date DATE,
    end_date DATE,
    currency VARCHAR(5) DEFAULT 'KZT',
    user_iin VARCHAR(12) NOT NULL,
    PRIMARY KEY (account_id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE finance.transactions (
	transaction_id SMALLINT NOT NULL AUTO_INCREMENT,
    transaction_card VARCHAR(20) NOT NULL,
    transaction_sum DOUBLE,
    transaction_date DATE,
    currency VARCHAR(5) DEFAULT 'KZT',
    account_id SMALLINT,
    PRIMARY KEY (transaction_id),
	CONSTRAINT FK_AccountTransaction FOREIGN KEY (account_id)
	REFERENCES finance.accounts (account_id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

