INSERT INTO finance.users (user_login, user_password, user_IIN, user_name, user_lastname, user_email, user_phone) 
VALUES ('sunny', 'pass', '970809300425', 'Sandra', 'Bullock', 'sandra@gmail.com', '+77018889990');
INSERT INTO finance.users (user_login, user_password, user_IIN, user_name, user_lastname, user_email, user_phone) 
VALUES ('actor', 'actor777', '670329301423', 'Johnny', 'Depp', 'john@gmail.com', '+77018889900');
INSERT INTO finance.users (user_login, user_password, user_IIN, user_name, user_lastname, user_email, user_phone) 
VALUES ('beyonce', 'singer777', '810709500505', 'Beyonce', 'Knowles', 'beyonce@gmail.com', '+77018889999');
INSERT INTO finance.users (user_login, user_password, user_IIN, user_name, user_lastname, user_email, user_phone) 
VALUES ('dimashi', 'singer555', '940524300400', 'Димаш', 'Кудайбергенов', 'dimashi@gmail.com', '+77018880000');
INSERT INTO finance.users (user_login, user_password, user_IIN, user_name, user_lastname, user_email, user_phone) 
VALUES ('john', 'man', '920520100900', 'John', 'Adams', 'john@gmail.com', '+77018880870');
INSERT INTO finance.users (user_login, user_password, user_IIN, user_name, user_lastname, user_email, user_phone) 
VALUES ('moon', 'moon111', '900411100800', 'Айнур', 'Жаркынова', 'moon@gmail.com', '+77057770870');


INSERT INTO finance.managers (manager_login, manager_password, manager_full_name, manager_phone, manager_email, manager_branch) 
VALUES ('Assek', 'abc123', 'Асель Саржанова', '+77273332211', 'asselek88@gmail.com', 'Орбита 3 отделение 12/1');
INSERT INTO finance.managers (manager_login, manager_password, manager_full_name, manager_phone, manager_email, manager_branch) 
VALUES ('Manager', 'manager777', 'Игорь Крутой', '+77273332200', 'ighor@gmail.com', 'Абылай хана 23 отделение 8/5');

INSERT INTO finance.accounts (account_number, card_number, card_type, start_date, end_date, currency, user_iin)
VALUES ('KZ400030003F90800088', '5450600060007000', 'ZP_card', '2019-08-08', '2022-05-11', 'KZT', '940524300400');
INSERT INTO finance.accounts (account_number, card_number, card_type, start_date, end_date, currency, user_iin)
VALUES ('KZ4004F0003090279000', '5450450067008900', 'ZP_card', '2020-05-12', '2024-05-11', 'KZT', '670329301423');
INSERT INTO finance.accounts (account_number, card_number, card_type, start_date, end_date, currency, user_iin)
VALUES ('KZ3009F6794500980077', '4450870096758900', 'VISA_card', '2020-05-12', '2024-05-11', 'KZT', '900411100800');
INSERT INTO finance.accounts (account_number, card_number, card_type, start_date, end_date, currency, user_iin)
VALUES ('KZ35678F500888445599', '5450800090008890', 'MASTER_card', '2020-03-10', '2024-12-12', 'KZT', '920520100900');
INSERT INTO finance.accounts (account_number, card_number, card_type, start_date, end_date, currency, user_iin)
VALUES ('KZ4004F0003090279890', '5450450065000000', 'VISA_card', '2020-01-01', '2025-05-01', 'KZT', '670329301423');

INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2019-11-25', 'KZT', 1);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2019-12-25', 'KZT', 1);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2020-01-25', 'KZT', 1);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2020-02-25', 'KZT', 1);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2020-03-25', 'KZT', 1);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2020-04-25', 'KZT', 1);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450600060007000', 360000, '2020-05-25', 'KZT', 1);


INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450067008900', 230000, '2020-03-25', 'KZT', 2);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450067008900', 240000, '2020-04-25', 'KZT', 2);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450067008900', 250000, '2020-05-25', 'KZT', 2);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450065000000', 45000, '2020-01-13', 'KZT', 5);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450065000000', 56000, '2020-02-13', 'KZT', 5);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450065000000', 70000, '2020-03-13', 'KZT', 5);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450065000000', 80000, '2020-04-13', 'KZT', 5);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('5450450065000000', 45000, '2020-05-13', 'KZT', 5);

INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('4450870096758900', 145000, '2020-01-13', 'KZT', 3);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('4450870096758900', 150000, '2020-02-13', 'KZT', 3);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('4450870096758900', 170000, '2020-03-13', 'KZT', 3);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('4450870096758900', 145000, '2020-04-13', 'KZT', 3);
INSERT INTO finance.transactions (transaction_card, transaction_sum, transaction_date, currency, account_id)
VALUES ('4450870096758900', 165000, '2020-05-13', 'KZT', 3);



