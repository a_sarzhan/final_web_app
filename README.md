# final_web_app
Проект "Микрокредитование":
1) Пользователь(клиент) может:
- войти в систему
- зарегистрироваться как новый пользотатель
- отправить заявку на получение кредита
- отредактировать личные данные
- изменить данные по кредитной заявке (в случае если заявка еще не рассмотрена), либо удалить заявку
- просмотреть предложенные условия по кредитованию по заявке и принять решение

2) Менеджер может:
- войти в систему
- провести поиск заявок по разным параметрам
- выбрать активные заявки и отправить на автоматическое принятие решении

Для запуска проекта необходимо:
СУБД MYSQL
- установить MySQL по ссылке https://dev.mysql.com/downloads/mysql/
- в конфигурационном файле my.ini под [mysqld] указать default-time-zone=+06:00 (г.Нур-Султан)
- необходимо создать тестовое БД по скрипту final-app DB.sql
- для добавления данных использовать скрипт final-app INSERT.sql

Tomcat Server (8.5)
- установить по ссылке https://tomcat.apache.org/download-80.cgi
- в папку lib необходимо поместить следующие библиотеки:
				1) log4j-api-2.13.1 (любую другую версию) - https://logging.apache.org/log4j/2.x/download.html
				2) log4j-core-2.13.1
				3) mysql-connector-java-8.0.20            - https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.20
				4) jstl-1.2								  - https://mvnrepository.com/artifact/javax.servlet/jstl/1.2
- deployment ways:
1) через Tomcat Manager, в конфигурационном файле tomcat-users.xml прописать:
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <user username="admin" password="password" roles="manager-gui, manager-script"/>
  http://localhost:8087/ - войти в Tomcat Manager, выбрав war file проекта развернуть 
2) через maven, командой: mvn tomcat7:deploy
После развертывания на Tomcat сервер, приложение будет доступно по ссылке 
  http://localhost:8087/project

